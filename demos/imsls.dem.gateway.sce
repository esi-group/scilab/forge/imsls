// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

demopath = get_absolute_file_path("imsls.dem.gateway.sce");
subdemolist = [
"wathen", "wathen.sce"; ..
"nonsym", "nonsym.sce"; ..
"nos1", "nos1.sce"; ..
"nos3", "nos3.sce"; ..
"pde225", "pde225.sce"; ..
"Poisson", "Poisson.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
