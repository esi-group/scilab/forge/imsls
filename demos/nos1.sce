// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mode(-1);
printf("Case with real sparse matrix");

mprintf("NOS1: Lanczos with partial reorthogonalization\n")
mprintf("Finite element approximation to biharmonic operator \n")
mprintf("on a beam with one end free and one end fixed. 80 elements with 3 DOF per node.\n")
mprintf("http://math.nist.gov/MatrixMarket/data/Harwell-Boeing/lanpro/nos1.html\n")

path = imsls_getpath();

printf("Loading matrix...\n")
datafile = fullfile(path,"tests","unit_tests","nos1.mtx");
printf("  File: %s\n",datafile)
A=mmread(datafile);

n=size(A,1);
lfil=15;
drop=1e-2;
M = eye(n,n);
M1 = eye(n,n);
M2 = eye(n,n);
max_it=250 ;
tol=1e-6 ;
x0=[];
xex=ones(n,1);
b=A*xex ;
restrt=20;
pltmode = %t;

imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol,restrt,pltmode);
