// Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, err, iter, flag, res]  = imsls_jacobi(varargin)
    // Solves linear equations using Jacobi Method.
    //
    // Calling Sequence
    // x  = imsls_jacobi(A, b)
    // x  = imsls_jacobi(A, b, x0)
    // x  = imsls_jacobi(A, b, x0, max_it)
    // x  = imsls_jacobi(A, b, x0, max_it, tol)
    // [x, err]  = imsls_jacobi(...)
    // [x, err, iter]  = imsls_jacobi(...)
    // [x, err, iter, flag]  = imsls_jacobi(...)
    // [x, err, iter, flag, res]  = imsls_jacobi(...)
    //
    // Parameters
    //    A:        a n-ny-n full or sparse nonsymmetric matrix of doubles
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))
    //    max_it:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)
    //    tol:      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)
    //    x:        a n-ny-1 full or sparse matrix of doubles,  solution vector
    //    err:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed
    //    flag:     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given max_it
    //    res:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    // Description
    // Solves the linear system Ax=b using the Jacobi Method.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
	//
    // It is not possible to provide A as a function for imsls_jacobi.
	// This is because the solver internally splits the matrix A into M and N, then
	// uses these matrices directly.
    //
    // The Jacobi method is based on solving for every variable locally with respect to the
    // other variables; one iteration of the method corresponds to solving for every variable
    // once.
    // The resulting method is easy to understand and implement, but convergence is
    // slow
    //
    // Examples
    // A=imsls_makefish(4);
    // xe=(1:16)';
    // b=A*xe;
    // [x,err,iter,flag,res] = imsls_jacobi(A,b)
    //
    // // With initial guess
    // x0=zeros(16,1);
    // [x,err,iter,flag,res] = imsls_jacobi(A,b,x0);
    //
    // // Convergence
    // // Increase max_it
    // // Use the default x0
    // max_it=200;
    // tol=1000*%eps;
    // [x,err,iter,flag,res] = imsls_jacobi(A,b,[],max_it,tol);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//jacobi.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin


    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_jacobi" , rhs , 2:5 )
    apifun_checklhs ( "imsls_jacobi" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    x0 = apifun_argindefault(varargin , 3 , zeros(n,1) );
    max_it = apifun_argindefault(varargin , 4 , n )
    tol = apifun_argindefault(varargin , 5 , 1000*%eps )
    //
    // Check types
    apifun_checktype ( "imsls_jacobi" , A ,  "A" , 1 , ["constant" "sparse"])
    apifun_checktype ( "imsls_jacobi" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_jacobi" , x0 , "x0" , 3 , ["constant" "sparse"])
    apifun_checktype ( "imsls_jacobi" , max_it , "max_it" , 4 , "constant")
    apifun_checktype ( "imsls_jacobi" , tol ,    "tol" , 5 , "constant")
    //
    // Check size
    apifun_checksquare ( "imsls_jacobi" , A , "A" , 1 )
    apifun_checkveccol ( "imsls_jacobi" , b , "b" , 2 , size(b,"*") )
    apifun_checkveccol ( "imsls_jacobi" , x0 , "x0" , 3 , size(x0,"*") )
    apifun_checkscalar ( "imsls_jacobi" , max_it , "max_it" , 4 )
    apifun_checkscalar ( "imsls_jacobi" , tol , "tol" , 5 )
    //
    // Check content
    apifun_checkgreq ( "imsls_jacobi" , max_it , "max_it" , 4 , 1 )
	apifun_checkflint( "imsls_jacobi" , max_it , "max_it" , 4)
    apifun_checkgreq ( "imsls_jacobi" , tol , "tol" , 5 , number_properties("tiny") )
    //
    // Setup the Matrix-Vector product A into a couple (function,list-of-args).
    [myA_fun,myA_args] = imsls_setupMatvec(A,imsls_matvec);

	// initialization
    i = 0;
	iter = 0;
    flag = 0;
    x = x0;

    bnrm2 = norm( b );
    if  ( bnrm2 == 0.0 ) then
        bnrm2 = 1.0;
    end

    r = b - A*x;
	
    err = norm( r ) / bnrm2;
    res = err;
    if ( err < tol ) then
        return;
    end

    [m,n]=size(A);
    [ M, N ] = imsls_split( A , b, 1.0, 1 );              // matrix splitting

    for i = 1:max_it,                            // begin iteration

        x_1 = x;
        x   = M \ (N*x + b);                         // update approximation

        err = norm( x - x_1 ) / norm( x );         // compute error
        res = [res;err];
        if ( err <= tol ) then
            iter=i;
            break;
        end              // check convergence

        if ( i == max_it ) then
            iter=i;
        end
    end

    if ( err > tol ) then
        flag = 1;
    end                // no convergence

    if ( flag <> 0 ) then
        warning(msprintf(gettext("%s: Algorithm has not converged."),"imsls_jacobi"))
    end
endfunction

