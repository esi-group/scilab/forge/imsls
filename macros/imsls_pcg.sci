// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, err, iter, flag, res] = imsls_pcg(varargin)
    // Solves linear equations using Conjugate Gradient method with preconditioning.
    //
    // Calling Sequence
    //  x = imsls_pcg(A, b)
    //  x = imsls_pcg(A, b, x0)
    //  x = imsls_pcg(A, b, x0, M1)
    //  x = imsls_pcg(A, b, x0, M1, M2)
    //  x = imsls_pcg(A, b, x0, M1, M2, max_it)
    //  x = imsls_pcg(A, b, x0, M1, M2, max_it, tol)
    //  [x, err] = imsls_pcg(...)
    //  [x, err, iter] = imsls_pcg(...)
    //  [x, err, iter, flag] = imsls_pcg(...)
    //  [x, err, iter, flag, res] = imsls_pcg(...)
    //
    // Parameters
    //    A:        a n-ny-n full or sparse matrix of doubles, symmetric positive definite matrix or function returning <literal>A*x</literal> or a list.
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))
    //    M1:       a n-ny-n full or sparse matrix of doubles or function or list, the left preconditioner matrix (default eye(n,n)) or function returning <literal>M1\x</literal> or a list.
    //    M2:       a n-ny-n full or sparse matrix of doubles or function or list, the right preconditioner matrix (default eye(n,n)) or function returning <literal>M2\x</literal> or a list.
    //    max_it:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)
    //    tol:      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)
    //    x:        a n-ny-1 full or sparse matrix of doubles,  solution vector
    //    err:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed
    //    flag:     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given max_it
    //    res:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    // Description
    // Solves the symmetric positive definite linear system Ax=b
    // using the Conjugate Gradient method with preconditioning.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
	//
	// The argument A can be a function returning <literal>A*x</literal>.
	// In this case, the function A must have the header :
    //   <programlisting>
    //     y = A ( x )
    //   </programlisting>
    //   where x is the current vector and y=A*x.
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument A can also be the list (funA,a1,a2,...).
    // In this case funA, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funA ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
	//
	// The argument M1 can be a function returning <literal>M1\x</literal>.
	// In this case, the function M1 must have the header :
    //   <programlisting>
    //     y = M1 ( x )
    //   </programlisting>
    // where x in the current vector and y=M1\x.
	//
	// The argument M1 can be the list (funM1,a1,a2,...).
    // In this case funM1, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funM1 ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
	//
	// The same feature is available for M2.
    //
    // The conjugate gradient method derives its name from the fact that it generates
    // a sequence of conjugate (or orthogonal) vectors.
    // These vectors are the residuals of the iterates.
    // They are also the gradients of a quadratic functional, the minimization of which
    // is equivalent to solving the linear system.
    // CG is an extremely effective method when
    // the coefficient matrix is symmetric positive deﬁnite, since storage for only a limited
    // number of vectors is required.
    //
    // Examples
    // // No convergence with default settings
    // A=imsls_lehmer(16);
    // xe = (1:16)';
    // b=A*xe;
    // [x,err,iter,flag,res] = imsls_pcg(A,b)
    // // Plot Iterations vs 2-norm Residual
    // scf();
    // plot(1:iter+1,log10(res),"b*-");
    // xtitle("Convergence of PCG","Iterations","Logarithm of 2-norm of residual")
    //
    // // With initial guess
    // x0=zeros(16,1);
    // [x,err,iter,flag,res] = imsls_pcg(A,b,x0);
    //
    // // With preconditionner
    // // Increase max_it to get convergence.
    // // Set x0 to [] to get the default.
    // M=eye(16,16);
    // max_it=100;
    // tol=1000*%eps;
    // [x,err,iter,flag,res] = imsls_pcg(A,b,[],M,[],max_it,tol);
    //
    // // Configure callbacks
    // function y=matvec(x)
    //     y=imsls_lehmer(16)*x
    // endfunction
    // function y=precond(x)
    //     y=2*eye(16,16)\x
    // endfunction
    //
    // [x,err,iter,flag,res] = imsls_pcg(A,b,x0,precond);
    // [x,err,iter,flag,res] = imsls_pcg(matvec,b,x0,M);
    // [x,err,iter,flag,res] = imsls_pcg(matvec,b,x0,precond,[],max_it,tol);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//cg.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_pcg" , rhs , 2:7 )
    apifun_checklhs ( "imsls_pcg" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    x0 = apifun_argindefault(varargin , 3 , zeros(n,1) );
    M1 = apifun_argindefault(varargin , 4 , [] );
    M2 = apifun_argindefault(varargin , 5 , [] );
    max_it = apifun_argindefault(varargin , 6 , n )
    tol = apifun_argindefault(varargin , 7 , 1000*%eps )
    //
    // Check types
    apifun_checktype ( "imsls_pcg" , A ,  "A" , 1 , ["constant" "sparse" "function" "list"])
    if ( typeof(A)=="list" ) then
        apifun_checktype ( "imsls_pcg" , A(1) ,  "A(1)" , 1 , "function" )
    end
    apifun_checktype ( "imsls_pcg" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_pcg" , x0 , "x0" , 3 , ["constant" "sparse"])
    apifun_checktype ( "imsls_pcg" , M1 ,  "M1" , 4 , ["constant" "sparse" "function" "list"])
    if ( typeof(M1)=="list" ) then
        apifun_checktype ( "imsls_pcg" , M1(1) ,  "M1(1)" , 4 , "function" )
    end
    apifun_checktype ( "imsls_pcg" , M2 , "M2" , 5 , ["constant" "sparse" "function" "list"])
    if ( typeof(M2)=="list" ) then
        apifun_checktype ( "imsls_pcg" , M2(1) ,  "M2(1)" , 5 , "function" )
    end
    apifun_checktype ( "imsls_pcg" , max_it , "max_it" , 6 , "constant")
    apifun_checktype ( "imsls_pcg" , tol ,    "tol" , 7 , "constant")
    //
    // Check size
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        apifun_checksquare ( "imsls_pcg" , A , "A" , 1 )
    end
    apifun_checkveccol ( "imsls_pcg" , b , "b" , 2 , size(b,"*") )
    apifun_checkveccol ( "imsls_pcg" , x0 , "x0" , 3 , size(x0,"*") )
    if ( or ( typeof(M1) == ["constant" "sparse"]) ) then
        if ( M1<> [] ) then
            apifun_checkdims ( "imsls_pcg" , M1 , "M1" , 4 , [n n] )
        end
    end
    if ( or ( typeof(M2) == ["constant" "sparse"]) ) then
        if ( M2<> [] ) then
            apifun_checkdims ( "imsls_pcg" , M2 , "M2" , 5 , [n n] )
        end
    end
    apifun_checkscalar ( "imsls_pcg" , max_it , "max_it" , 6 )
    apifun_checkscalar ( "imsls_pcg" , tol , "tol" , 7 )
    //
    // Check content
    apifun_checkgreq ( "imsls_pcg" , max_it , "max_it" , 6 , 1 )
	apifun_checkflint( "imsls_pcg" , max_it , "max_it" , 6)
    apifun_checkgreq ( "imsls_pcg" , tol , "tol" , 7 , number_properties("tiny") )
    //
    // Extra-checks
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        if ( bool2s(or( A ~= A')) == 1 ) then
            warning(msprintf(gettext("%s: matrix A should be symetric"),"imsls_pcg"));
        end
    end
    //
    // Setup the Matrix-Vector product A into a couple (function,list-of-args).
    [myA_fun,myA_args] = imsls_setupMatvec(A,imsls_matvec);
    //
    // Setup the Left Preconditionner M1
    [myM1_fun,myM1_args] = imsls_setupPrecond(M1,imsls_precondfake,imsls_precond);
    //
    // Setup the Right Preconditionner M2
    [myM2_fun,myM2_args] = imsls_setupPrecond(M2,imsls_precondfake,imsls_precond);

	// initialization
    flag = 0;
	iter = 0;
    i = 0;
    x = x0;

    bnrm2 = norm( b );
    if  ( bnrm2 == 0.0 ) then
        bnrm2 = 1.0;
    end

    //  r = b - A*x;
    r = b - myA_fun(x,myA_args(1:$));
    err = norm( r ) / bnrm2;
    res = err;
    if ( err < tol ) then
        return;
    end

    for i = 1:max_it,                       // begin iteration

        //     z  = (M1*M2) \ r; decomposed into:
        // y= M1\r;
        y = myM1_fun(r,myM1_args(1:$));
        // z= M2\y;
        z = myM2_fun(y,myM2_args(1:$));
        rho = (r'*z);

        if ( i > 1 ) then                       // direction vector
            bet = rho / rho_1;
            p = z + bet*p;
        else
            p = z;
        end

        //     q = A*p;

        q=myA_fun(p,myA_args(1:$))
        alppha = rho / (p'*q );
        x = x + alppha * p;                    // update approximation vector

        r = r - alppha*q;                      // compute residual
        err = norm( r ) / bnrm2;            // check convergence
        res = [res;err];
        if ( err <= tol ) then
            iter=i;
            break;
        end

        rho_1 = rho;

        if ( i == max_it ) then
            iter=i;
        end

    end

    if ( err > tol ) then
        flag = 1;
    end         // no convergence

    if ( flag <> 0 ) then
        warning(msprintf(gettext("%s: Algorithm has not converged."),"imsls_pcg"))
    end
endfunction
