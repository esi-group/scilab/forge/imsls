// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function imsls_tester(  )
    // Test all algorithms
    //
    // Calling Sequence
    // imsls_tester(  )
    //
    // Parameters
    //
    // Description
    // Loops over the various iterative algorithms.
    // Test matrices are generated in imsls_matgen.
    // Results are printed to the screen.
    //
    // Examples
    //  imsls_tester(  )
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab/tester.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_tester" , rhs , 0 )
    apifun_checklhs ( "imsls_tester" , lhs , 0:1 )

    no_soln_jac   = 0;                     // convergence check
    no_soln_sor   = 0;
    no_soln_pcg    = 0;
    no_soln_cheby = 0;
    no_soln_gmres = 0;
    no_soln_bicg  = 0;
    no_soln_cgs   = 0;
    no_soln_bicgs = 0;
    no_soln_qmr   = 0;

    guess_err_jac   = 0;                   // initial guess = solution error
    guess_err_sor   = 0;
    guess_err_pcg    = 0;
    guess_err_cheby = 0;
    guess_err_gmres = 0;
    guess_err_bicg  = 0;
    guess_err_cgs   = 0;
    guess_err_bicgs = 0;
    guess_err_qmr   = 0;

    allpassed = 0;

    num_tests = 6;
    ep = %eps;
    tol = ep * 1000;

    //  BEGIN TESTING: FORM SYSTEM AND APPLY ALGORITHMS

    for test = 1:num_tests
        mprintf("Test %d/%d\n",test,num_tests)
        A = imsls_matgen( test*10 );             // form test matrix
        [sizeA,sizeA] = size(A);
        max_it = sizeA * 10;
        normA = norm( A,%inf );
        if ( test == 1 | test == 2 | test == 3 | test == 6 ) then
            // set rhs = row sums
            b = sum(A,"c");
        else
            b = ones(sizeA,1);              // set rhs = unit vector
        end
        if ( test < 4 ) then
            M = eye(sizeA,sizeA);                 // no preconditioning
        else
            M = diag(diag(A));              // diagonal preconditioning
        end

        if ( test < 6 ) then
            xk = 0*b;                       // initial guess = zero vector
        else
            xk = ones(sizeA,1 );            // initial guess = solution
        end

        //     TEST EACH METHOD; CHECK ACCURACY IF CONVERGENCE CLAIMED

        //     Test Jacobi and Chebyshev

        if ( test == 1 | test == 6 ) then
            mprintf("    imsls_jacobi...\n")
            jac_maxit = max_it * 3;
            [x, err, iter, flag_jac] = imsls_jacobi(A, b, xk, jac_maxit, tol);
            if ( flag_jac ~= 0 & test ~= 6 ) then
                no_soln_jac = no_soln_jac + 1;
                printf("jacobi failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_jac ~= 0 )
                guess_err_jac = guess_err_jac + 1;
                printf("jacobi failed for initial guess = solution\n");
                disp(test, iter, flag_jac)
                allpassed = allpassed + 1;
            end

            mprintf("    imsls_cheby...\n")
            [x, err, iter, flag_cheby] = imsls_cheby(A, b, xk, M, max_it, tol);
            if ( flag_cheby ~= 0 & test ~= 6 ) then
                no_soln_cheby = no_soln_cheby + 1;
                printf("chebyshev failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_cheby ~= 0 )
                guess_err_cheby = guess_err_cheby + 1;
                printf("chebyshev failed for initial guess = solution\n");
                disp(test, iter, flag_cheby)
                allpassed = allpassed + 1;
            end

        end

        //     SPD ROUTINES

        if ( test == 1 | test == 2 | test == 4 | test == 6 ) then

            if ( test == 1 ) then                     // various relaxation parameters
                w = 1.0;
            elseif ( test == 2 ) then
                w = 1.2;
            else
                w = 1.1;
            end

            mprintf("    imsls_sor...\n")
            sor_maxit = max_it * 3;
            [x, err, iter, flag_sor] = imsls_sor(A, b, xk, w, sor_maxit, tol);
            if ( flag_sor ~= 0 & test ~= 6 ) then
                no_soln_sor = no_soln_sor + 1;
                printf("sor failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_sor ~= 0 ) then
                guess_err_sor = guess_err_sor + 1;
                printf("sor failed for initial guess = solution\n");
                disp(test, iter, flag_sor)
                allpassed = allpassed + 1;
            end

            mprintf("    imsls_pcg...\n")
            [x, err, iter, flag_pcg] = imsls_pcg(A, b, xk, M, [], max_it, tol);
            if ( flag_pcg ~= 0 & test ~= 6 ) then
                no_soln_pcg = no_soln_pcg + 1;
                printf("pcg failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_pcg ~= 0 ) then
                guess_err_pcg = guess_err_pcg + 1;
                printf("pcg failed for initial guess = solution\n");
                disp(test, iter, flag_pcg)
                allpassed = allpassed + 1;
            end
        end

        //     Nonsymmetric ROUTINES

        if ( test == 1 | test == 4 | test == 5 | test == 6 ) then

            restrt = test*10;
            if ( restrt == 0 ) then
                restrt = 1;
            end;
            mprintf("    imsls_gmres...\n")
            [x, err, iter, flag_gmres]=imsls_gmres( A, b, xk, M, [], restrt, max_it, tol );
            if ( flag_gmres ~= 0 & test ~= 6 ) then
                no_soln_gmres = no_soln_gmres + 1;
                printf("gmres failed to converge for\n");
                disp(test)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_gmres ~= 0 ) then
                guess_err_gmres = guess_err_gmres + 1;
                printf("gmres failed for initial guess = solution\n");
                disp(test, iter, flag_gmres)
                allpassed = allpassed + 1;
            end
            mprintf("    imsls_bicg...\n")
            [x, err, iter, flag_bicg] = imsls_bicg(A, b, xk, M, [], max_it, tol);
            if ( flag_bicg ~= 0 & test ~= 6 ) then
                no_soln_bicg = no_soln_bicg + 1;
                printf("bicg failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_bicg ~= 0 )
                guess_err_bicg = guess_err_bicg + 1;
                printf("bicg failed for initial guess = solution\n");
                disp(test, iter, flag_bicg)
                allpassed = allpassed + 1;
            end

            mprintf("    imsls_cgs...\n")
            [x, err, iter, flag_cgs] = imsls_cgs(A, b, xk, M, [], max_it, tol);
            if ( flag_cgs ~= 0 & test ~= 6 ) then
                no_soln_cgs = no_soln_cgs + 1;
                printf("cgs failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_cgs ~= 0 )
                guess_err_cgs = guess_err_cgs + 1;
                printf("cgs failed for initial guess = solution\n");
                disp(test, iter, flag_cgs)
                allpassed = allpassed + 1;
            end

            mprintf("    imsls_bicgstab...\n")
            [x, err, iter, flag_bicgs] = imsls_bicgstab(A, b, xk, M, [], max_it, tol);
            if ( flag_bicgs ~= 0 & test ~= 6 ) then
                no_soln_bicgs = no_soln_bicgs + 1;
                printf("bicgstab failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_bicgs ~= 0 ) then
                guess_err_bicgs = guess_err_bicgs + 1;
                printf("bicgstab failed for initial guess = solution\n");
                disp(test, iter, flag_bicgs)
                allpassed = allpassed + 1;
            end

            mprintf("    imsls_qmr...\n")
            [x, err, iter, flag_qmr] = imsls_qmr(A, b, xk, M, M, max_it, tol);
            if ( flag_qmr ~= 0 & test ~= 6 ) then
                no_soln_qmr = no_soln_qmr + 1;
                printf("qmr failed to converge for\n");
                disp(test, err)
                allpassed = allpassed + 1;
            end
            if ( test == 6 & iter ~= 0 & flag_qmr ~= 0 ) then
                guess_err_qmr = guess_err_qmr + 1;
                printf("qmr failed for initial guess = solution\n");
                disp(test, iter, flag_qmr)
                allpassed = allpassed + 1;
            end
        end
    end

    //  REPORT RESULTS

    TESTING = printf("TEST SUITE COMPLETE\n");

    if ( allpassed == 0 ) then
        RESULTS = printf("ALL TESTS PASSED\n");
    end

    if ( no_soln_jac ~= 0 ) then
        printf("jacobi failed test (failed to converge)\n");
    elseif ( guess_err_jac ~= 0 ) then
        printf("jacobi failed test (initial guess = solution error)\n");
    else
        printf("jacobi passed test\n");
    end

    if ( no_soln_sor ~= 0 ) then
        printf("sor failed test (failed to converge)\n");
    elseif ( guess_err_sor ~= 0 ) then
        printf("sor failed test (initial guess = solution error)\n");
    else
        printf("sor passed test\n");
    end

    if ( no_soln_pcg ~= 0 ) then
        printf("pcg failed test (failed to converge)\n");
    elseif ( guess_err_pcg ~= 0 ) then
        printf("pcg failed test (initial guess = solution error)\n");
    else
        printf("pcg passed test\n");
    end

    if ( no_soln_cheby ~= 0 ) then
        printf("cheby failed test (failed to converge)\n");
    elseif ( guess_err_cheby ~= 0 ) then
        printf("cheby failed test (initial guess = solution error)\n");
    else
        printf("cheby passed test\n");
    end

    if ( no_soln_gmres ~= 0 ) then
        printf("gmres failed test (failed to converge)\n");
    elseif ( guess_err_gmres ~= 0 ) then
        printf("gmres failed test (initial guess = solution error)\n");
    else
        printf("gmres passed test\n");
    end

    if ( no_soln_bicg ~= 0 ) then
        printf("bicg failed test (failed to converge)\n");
    elseif ( guess_err_bicg ~= 0 ) then
        printf("bicg failed test (initial guess = solution error)\n");
    else
        printf("bicg passed test\n");
    end

    if ( no_soln_cgs ~= 0 ) then
        printf("cgs failed test (failed to converge)\n");
    elseif ( guess_err_cgs ~= 0 ) then
        printf("cgs failed test (initial guess = solution error)\n");
    else
        printf("cgs passed test\n");
    end

    if ( no_soln_bicgs ~= 0 ) then
        printf("bicgstab failed test (failed to converge)\n");
    elseif ( guess_err_bicgs ~= 0 ) then
        printf("bicgstab failed test (initial guess = solution error)\n");
    else
        printf("bicgstab passed test\n");;
    end

    if ( no_soln_qmr ~= 0 ) then
        printf("qmr failed test (failed to converge)\n");
    elseif ( guess_err_qmr ~= 0 ) then
        printf("qmr failed test (initial guess = solution error)\n");
    else
        printf("qmr passed test\n");
    end
endfunction
