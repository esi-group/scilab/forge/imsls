// Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, err, iter, flag, res] = imsls_cheby(varargin)
    // Solves linear equations using Chebyshev Method with preconditioning.
    //
    // Calling Sequence
    // x = imsls_cheby(A, b)
    // x = imsls_cheby(A, b, x0)
    // x = imsls_cheby(A, b, x0, M)
    // x = imsls_cheby(A, b, x0, M, max_it)
    // x = imsls_cheby(A, b, x0, M, max_it, tol)
    // [x, err] = imsls_cheby(...)
    // [x, err, iter] = imsls_cheby(...)
    // [x, err, iter, flag] = imsls_cheby(...)
    // [x, err, iter, flag, res] = imsls_cheby(...)
    //
    // Parameters
    //    A:        a n-ny-n full matrix of doubles, symmetric positive definite matrix
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default zeros(n,1))
    //    M:        a n-ny-n full or sparse matrix of doubles, preconditioner matrix (default eye(n,n))
    //    max_it:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)
    //    tol       a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)
    //    x:        a n-ny-1 full or sparse matrix of doubles,  solution vector
    //    err:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed
    //    flag:     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given max_it
    //    res:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    //
    // Description
    // Solves the symmetric positive definite linear system Ax=b
    // using the Chebyshev Method with preconditioning.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
	//
	// For this function, it is not possible to provide A or M as a function.
	// This is because the algorithm requires to compute spec(inv(M)*A), so that
	// both A and M must be known explicitly.
	//
	// This function requires A to be a full matrix.
	// This is because we use the spec function to compute the eigenvalues
	// of a matrix: the spec function only works for full matrices.
	//
	// For symmetric positive definite systems, lambda_min and lambda_max are the smallest and
	// largest eigenvalues of M^-1 * A: this property is used in the current algorithm.
	//
    // The Chebyshev Iteration recursively determines polynomials with coefficients chosen
    // to minimize the norm of the residual in a min-max sense.
    // The coefficient matrix must be positive definite and knowledge of the extremal eigenvalues is required.
    // This method has the advantage of requiring no inner products.
    //
    // Examples
    // // Simple test: no convergence
    // A = imsls_makefish( 4 );
    // xe=(1:16)';
    // b=A*xe;
    // [x,err,iter,flag,res] = imsls_cheby(A,b)
    //
    // // With initial guess
    // // No convergence
    // x0=zeros(16,1);
    // [x,err,iter,flag,res] = imsls_cheby(A,b,x0);
    //
    // // With preconditionner
    // // Convergence: increase max_it
    // // Use default value for x0
    // M=eye(16,16);
    // max_it=100;
    // tol=1000*%eps;
    // [x,err,iter,flag,res] = imsls_cheby(A,b,[],M,max_it,tol);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//cheby.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin


    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_cheby" , rhs , 2:6 )
    apifun_checklhs ( "imsls_cheby" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    x0 = apifun_argindefault(varargin , 3 , zeros(n,1) );
    M = apifun_argindefault(varargin , 4 , [] );
    max_it = apifun_argindefault(varargin , 5 , n )
    tol = apifun_argindefault(varargin , 6 , 1000*%eps )
    //
    // Check types
    apifun_checktype ( "imsls_cheby" , A ,  "A" , 1 , ["constant" "sparse"])
    apifun_checktype ( "imsls_cheby" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_cheby" , x0 , "x0" , 3 , ["constant" "sparse"])
    apifun_checktype ( "imsls_cheby" , M ,  "M" , 4 , ["constant" "sparse"])
    apifun_checktype ( "imsls_cheby" , max_it , "max_it" , 5 , "constant")
    apifun_checktype ( "imsls_cheby" , tol ,    "tol" , 6 , "constant")
    //
    // Check size
    apifun_checksquare ( "imsls_cheby" , A , "A" , 1 )
    apifun_checkveccol ( "imsls_cheby" , b , "b" , 2 , size(b,"*") )
    apifun_checkveccol ( "imsls_cheby" , x0 , "x0" , 3 , size(x0,"*") )
        if ( M<> [] ) then
            apifun_checkdims ( "imsls_cheby" , M , "M" , 4 , [n n] )
        end
    apifun_checkscalar ( "imsls_cheby" , max_it , "max_it" , 5 )
    apifun_checkscalar ( "imsls_cheby" , tol , "tol" , 6 )
    //
    // Check content
    apifun_checkgreq ( "imsls_cheby" , max_it , "max_it" , 5 , 1 )
	apifun_checkflint( "imsls_cheby" , max_it , "max_it" , 5)
    apifun_checkgreq ( "imsls_cheby" , tol , "tol" , 6 , number_properties("tiny") )
    //
    // Extra-checks
        if ( bool2s(or( A ~= A')) == 1 ) then
            warning(msprintf(gettext("%s: matrix A should be symetric"),"imsls_cheby"));
        end
    //
    if ( M==[] ) then
    	M = eye(A)
    end

    // initialization
    i = 0;
	iter = 0
    flag = 0;
    x = x0;

    bnrm2 = norm( b );
    if  ( bnrm2 == 0.0 ) then
        bnrm2 = 1.0;
    end

    r = b - A*x;

    err = norm( r ) / bnrm2;
    res = err;
    if ( err < tol ) then
        return;
    end

    eigs = spec( inv(M)*A );
    // Extract real part, in the case where some of the
    // eigenvalues are imaginary.
    eigs = real(eigs)

    eigmax = max( eigs );
    eigmin = min( eigs );

    c = ( eigmax - eigmin ) / 2.0;
    d = ( eigmax + eigmin ) / 2.0;

    for i = 1:max_it,                     // begin iteration

        z =  M \ r;

        if ( i > 1 )                        // direction vectors
            bet = ( c*alppha / 2.0 )^2;
            alppha = 1.0 / ( d - bet );
            p = z + bet*p;
        else
            p = z;
            alppha = 2.0 / d;
        end

        x  = x + alppha*p;                      // update approximation

        r = r - alppha*A*p;

        err = norm( r ) / bnrm2;             // check convergence
        res = [res;err];
        if ( err <= tol  ) then
            iter=i;
            break;
        end

        if ( i == max_it ) then
            iter=i;
        end

    end

    if ( err > tol ) then
        flag = 1;
    end;        // no convergence

    if ( flag <> 0 ) then
        warning(msprintf(gettext("%s: Algorithm has not converged."),"imsls_cheby"))
    end
endfunction
