// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function mat = imsls_makefish(siz)
    // Returns the Poisson matrix.
    //
    // Calling Sequence
    //  A = imsls_makefish(siz)
    //
    // Parameters
    //    siz:       a 1-by-1 matrix of doubles, integer value
    //    A:         N-by-N symmetric positive definite matrix, where N=siz^2
    //
    // Description
    // Returns the symmetric positive definite N-by-N matrix.
    //
    //        A is totally nonnegative.
    //        inv(A) is tridiagonal, and explicit
    //        formulas are known for its entries.
    //        N <= COND(A) <= 4*N*N.
    //
    // Examples
    // A = imsls_makefish( 4 )
    //
    // Authors
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

	[lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_makefish" , rhs , 1 )
    apifun_checklhs ( "imsls_makefish" , lhs , 1 )
    //
    // Check types
    apifun_checktype ( "imsls_makefish" , siz ,  "siz" , 1 , "constant")
    //
    // Check size
    apifun_checkscalar ( "imsls_makefish" , siz ,  "siz" , 1 )
    //
    // Check content
    apifun_checkgreq ( "imsls_makefish" , siz ,  "siz" , 1 , 1 )
	apifun_checkflint( "imsls_makefish" , siz ,  "siz" , 1)

    // begin of computations

    leng = siz*siz;
    dia = zeros(siz,siz);
    off = -eye(siz,siz);
    for i=1:siz
        dia(i,i)=4;
    end
    for i=1:siz-1
        dia(i,i+1)=-1;
        dia(i+1,i)=-1;
    end
    mat = zeros(leng,leng);
    for ib=1:siz
        mat(1+(ib-1)*siz:ib*siz,1+(ib-1)*siz:ib*siz) = dia;
    end
    for ib=1:siz-1
        mat(1+(ib-1)*siz:ib*siz,1+ib*siz:(ib+1)*siz) = off;
        mat(1+ib*siz:(ib+1)*siz,1+(ib-1)*siz:ib*siz) = off;
    end

endfunction
