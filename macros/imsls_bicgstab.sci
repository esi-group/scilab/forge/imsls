// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, err, iter, flag, res] = imsls_bicgstab(varargin)
    // Solves linear equations using BiConjugate Gradient Stabilized Method with preconditioning.
    //
    // Calling Sequence
    //  x = imsls_bicgstab(A, b)
    //  x = imsls_bicgstab(A, b, x0)
    //  x = imsls_bicgstab(A, b, x0, M1)
    //  x = imsls_bicgstab(A, b, x0, M1, M2)
    //  x = imsls_bicgstab(A, b, x0, M1, M2, max_it)
    //  x = imsls_bicgstab(A, b, x0, M1, M2, max_it, tol)
    //  [x, err] = imsls_bicgstab(...)
    //  [x, err, iter] = imsls_bicgstab(...)
    //  [x, err, iter, flag] = imsls_bicgstab(...)
    //  [x, err, iter, flag, res] = imsls_bicgstab(...)
    //
    // Parameters
    //    A:        a n-ny-n full or sparse nonsymmetric matrix of doubles or function returning <literal>A*x</literal>
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))
    //    M1:        a n-ny-n full or sparse matrix of doubles, left preconditioner matrix (default: eye(n,n)) or function returning <literal>M1\x</literal>
    //    M2:        a n-ny-n full or sparse matrix of doubles, right preconditioner matrix (default: eye(n,n)) or function returning <literal>M2\x</literal>
    //    max_it:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)
    //    tol:      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)
    //    x:        a n-ny-1 full or sparse matrix of doubles,  solution vector
    //    err:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed
    //    flag:     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given max_it, -1: breakdown caused by rho = 0, -2: breakdown caused by omega = 0
    //    res:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    // Description
    // Solves the linear system Ax=b using the
    // BiConjugate Gradient Stabilized Method with preconditioning.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
    //
    // The Biconjugate Gradient Stabilized method is a variant of BiCG, like CGS, but using
    // different updates for the AT-sequence in order to obtain smoother convergence than
    // CGS.
	//
    // The argument A can be a function returning <literal>A*x</literal>.
    // In this case, the function A must have the header :
    //   <programlisting>
    //     y = A ( x )
    //   </programlisting>
    // where x is the current vector.
    // The A function must return y=A*x.
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument A can also be the list (funA,a1,a2,...).
    // In this case funA, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funA ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The argument M1 can be a function returning <literal>M1\x</literal>.
    // In this case, the function M1 must have the header :
    //   <programlisting>
    //     y = M1 ( x )
    //   </programlisting>
    // where x in the current vector.
    // The M1 function must return y=M1\x.
    //
    // The argument M1 can be the list (funM1,a1,a2,...).
    // In this case funM1, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funM1 ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The same feature is available for M2.
    //
    // Examples
    // A=imsls_makefish(4);
    // xe=(1:16)';
    // b=A*xe;
    // [x,err,iter,flag,res] = imsls_bicgstab(A,b)
    //
    // // With initial guess
    // x0=zeros(16,1);
    // [x,err,iter,flag,res] = imsls_bicgstab(A,b,x0);
    //
    // // With preconditionner
    // M1=eye(16,16);
    // M2=eye(16,16);
    // max_it=16;
    // tol=1000*%eps;
    // [x,err,iter,flag,res] = imsls_bicgstab(A,b,x0,M1,M2,max_it,tol);
    //
    // // With callbacks
    // function y=precondM1(x)
    //     y=2*eye(16,16)\x
    // endfunction
    // function y=matvec(x)
    //     y=imsls_makefish(4)*x
    // endfunction
    //
    // [x,err,iter,flag,res] = imsls_bicgstab(matvec,b,x0,precondM1,[],max_it,tol);
    //
    // [x,err,iter,flag,res] = imsls_bicgstab(A,b,x0,precondM1);
    // [x,err,iter,flag,res] = imsls_bicgstab(matvec,b,x0,M1);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab/bicgstab.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin


    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_bicgstab" , rhs , 2:7 )
    apifun_checklhs ( "imsls_bicgstab" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    x0 = apifun_argindefault ( varargin , 3 , zeros(n,1) );
    M1 = apifun_argindefault ( varargin , 4 , [] );
    M2 = apifun_argindefault ( varargin , 5 , [] );
    max_it = apifun_argindefault ( varargin , 6 , n )
    tol = apifun_argindefault ( varargin , 7 , 1000*%eps )
    //
    // Check types
    apifun_checktype ( "imsls_bicgstab" , A ,  "A" , 1 , ["constant" "sparse" "function" "list"])
    if ( typeof(A)=="list" ) then
        apifun_checktype ( "imsls_bicgstab" , A(1) ,  "A(1)" , 1 , "function" )
    end
    apifun_checktype ( "imsls_bicgstab" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_bicgstab" , x0 , "x0" , 3 , ["constant" "sparse"])
    apifun_checktype ( "imsls_bicgstab" , M1 ,  "M1" , 4 , ["constant" "sparse" "function" "list"])
    if ( typeof(M1)=="list" ) then
        apifun_checktype ( "imsls_bicgstab" , M1(1) ,  "M1(1)" , 4 , "function" )
    end
    apifun_checktype ( "imsls_bicgstab" , M2 ,  "M2" , 5 , ["constant" "sparse" "function" "list"])
    if ( typeof(M2)=="list" ) then
        apifun_checktype ( "imsls_bicgstab" , M2(1) ,  "M2(1)" , 5 , "function" )
    end
    apifun_checktype ( "imsls_bicgstab" , max_it , "max_it" , 6 , "constant")
    apifun_checktype ( "imsls_bicgstab" , tol ,    "tol" , 7 , "constant")
    //
    // Check size
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        apifun_checksquare ( "imsls_bicgstab" , A , "A" , 1 )
    end
    apifun_checkveccol ( "imsls_bicgstab" , b , "b" , 2 , size(b,"*") )
    apifun_checkveccol ( "imsls_bicgstab" , x0 , "x0" , 3 , size(x0,"*") )
    if ( or ( typeof(M1) == ["constant" "sparse"]) ) then
        if ( M1<> [] ) then
            apifun_checkdims ( "imsls_bicgstab" , M1 , "M1" , 4 , [n n] )
        end
    end
    if ( or ( typeof(M2) == ["constant" "sparse"]) ) then
        if ( M2<> [] ) then
            apifun_checkdims ( "imsls_bicgstab" , M2 , "M2" , 5 , [n n] )
        end
    end
    apifun_checkscalar ( "imsls_bicgstab" , max_it , "max_it" , 6 )
    apifun_checkscalar ( "imsls_bicgstab" , tol , "tol" , 7 )
    //
    // Check content
    apifun_checkgreq ( "imsls_bicgstab" , max_it , "max_it" , 6 , 1 )
	apifun_checkflint( "imsls_bicgstab" , max_it , "max_it" , 6)
    apifun_checkgreq ( "imsls_bicgstab" , tol , "tol" , 7 , number_properties("tiny") )
    //
    // Setup the Matrix-Vector product A into a couple (function,list-of-args).
    [myA_fun,myA_args] = imsls_setupMatvec(A,imsls_matvec);
    //
    // Setup the Left Preconditionner M1
    [myM1_fun,myM1_args] = imsls_setupPrecond(M1,imsls_precondfake,imsls_precond);
    //
    // Setup the Right Preconditionner M2
    [myM2_fun,myM2_args] = imsls_setupPrecond(M2,imsls_precondfake,imsls_precond);

    // initialization
    i = 0
    iter = 0;
    flag = 0;
    x = x0;

    bnrm2 = norm( b );
    if  ( bnrm2 == 0.0 ) then
        bnrm2 = 1.0;
    end

    //  r = b - A*x;
    r = b - myA_fun(x,myA_args(1:$));

    err = norm( r ) / bnrm2;
    res=err;
    if ( err < tol ) then
        return;
    end

    omega  = 1.0;
    r_tld = r;

    for i = 1:max_it                              // begin iteration

        rho   = ( r_tld'*r );                          // direction vector
        if ( rho == 0.0 ) then
            iter=i-1;
            break;
        end

        if ( i > 1 ),
            bet  = ( rho/rho_1 )*( alppha/omega );
            p = r + bet*( p - omega*v );
        else
            p = r;
        end

        //   p_hat = M2\(M1 \ p);
        p_hat = myM1_fun(p,myM1_args(1:$));
        p_hat = myM2_fun(p_hat,myM2_args(1:$));

        //   v = A*p_hat;
        v= myA_fun(p_hat,myA_args(1:$));
		
        alppha = rho / ( r_tld'*v );
        s = r - alppha*v;
        if ( norm(s) < tol ),                          // early convergence check
           x = x + alppha*p_hat;
           err = norm( s ) / bnrm2;
           res = [res;err];
           iter=i;
           break;
        end

        //   s_hat = M2\(M1 \ s);                                 // stabilizer
        s_hat = myM1_fun(s,myM1_args(1:$));
        s_hat = myM2_fun(s_hat,myM2_args(1:$));

        //   t = A*s_hat;
        t = myA_fun(s_hat,myA_args(1:$));

        omega = ( t'*s) / ( t'*t );

        x = x + alppha*p_hat + omega*s_hat;             // update approximation

        r = s - omega*t;
        err = norm( r ) / bnrm2;                      // check convergence
        res = [res;err];
		
        if ( err <= tol ) then
            iter=i;
            break;
        end

        if ( omega == 0.0 ) then
            iter=i;
            break;
        end
        rho_1 = rho;
        if ( i == max_it ) then
            iter=i;
        end

    end

    if ( err <= tol | s <= tol ) then                   // converged
        if ( s <= tol ) then
            err = norm(s) / bnrm2;
        end
        flag =  0;
    elseif ( omega == 0.0 ) then                          // breakdown
        flag = -2;
    elseif ( rho == 0.0 ) then
        flag = -1;
    else                                              // no convergence
        flag = 1;
    end

    if ( flag <> 0 ) then
        warning(msprintf(gettext("%s: Algorithm has not converged."),"imsls_bicg"))
    end
endfunction
