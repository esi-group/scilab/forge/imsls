// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [myA_fun,myA_args] = imsls_setupMatvec(A,defaultfun)
    // Setup the Matrix-Vector product A into a couple (function,list-of-args).
    //
	// A: a full or sparse matrix of doubles
	//    or a function performing A*x, with header y=f(x)
	//    or a list, where the first elements is f and the
	//    remaining elements are the extra-arguments of f.
	//    In this last case, the header is y=f(x,a1,...)
	//
	
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        myA_fun = defaultfun;
        myA_args = list(A);
    elseif ( typeof(A) == "function" ) then
        myA_fun = A;
        myA_args = list();
    else
        myA_fun = A(1);
        myA_args = list(A(2:$));
    end
endfunction
