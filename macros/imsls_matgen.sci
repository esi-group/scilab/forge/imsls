// Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function mat = imsls_matgen( siz )
    // Returns a test matrix.
    //
    // Calling Sequence
    // A = imsls_matgen( siz )
    //
    // Parameters
    //    siz:      a 1-by-1 matrix of doubles, integer value, available values are 10, 20, 30, 40, 50.
    //    A:      a n-by-n matrix of doubles
    //
    // Description
    // Returns a test matrix.
    // If siz=10, returns a 16-by-16 Poisson matrix, if siz=20, returns a 40-by-40 Wathen matrix,
    // if siz=30, returns a 40-by-40 Wathen matrix, if siz=40, returns a 5-by-5 Lehmer matrix,
    // if siz=50, returns a 10-by-10 Lehmer matrix.
    // For any other value of siz, returns a 16-by-16 Poisson matrix.
    //
    // Examples
    //  A = imsls_matgen( 10 )
    //  A = imsls_matgen( 20 )
    //  A = imsls_matgen( 30 )
    //  A = imsls_matgen( 40 )
    //  A = imsls_matgen( 50 )
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//gmres.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_matgen" , rhs , 1 )
    apifun_checklhs ( "imsls_matgen" , lhs , 1 )
    //
    // Check types
    apifun_checktype ( "imsls_matgen" , siz ,  "siz" , 1 , "constant")
    //
    // Check size
    apifun_checkscalar ( "imsls_matgen" , siz ,  "siz" , 1 )
    //
    // Check content
    apifun_checkgreq ( "imsls_matgen" , siz ,  "siz" , 1 , 1 )
	apifun_checkflint( "imsls_matgen" , siz ,  "siz" , 1)

    // begin of computations

    if ( siz == 10 ) then
        mat = imsls_makefish( 4 );           // poisson matrix
    elseif ( siz == 20  ) then
        mat = imsls_wathen( 3, 3, 0 );       // spd consistent mass matrix
    elseif ( siz == 30  ) then
        mat = imsls_wathen( 3, 3, 1 );       // spd consistent mass matrix
    elseif ( siz == 40 ) then
        mat = imsls_lehmer(5);
    elseif ( siz == 50 ) then
        mat = imsls_lehmer(10);
    else
        mat = imsls_makefish( 4 );           // irrelevant: x0 = exact solution
    end

    //END matgen.sci
endfunction
