// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function path = imsls_getpath (  )
    // Returns the path to the current module.
    //
    // Calling Sequence
    //   path = imsls_getpath()
    //
    // Parameters
    //   path : a 1-by-1 matrix of strings, the path to the current module.
    //
    // Examples
    //   path = imsls_getpath()
    //
    // Authors
    //   Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin

    [lhs rhs] = argn();
    apifun_checkrhs("imsls_getpath", rhs, 0:0);
    apifun_checklhs("imsls_getpath", lhs, 1:1);

    path = fileparts(get_function_path("imsls_getpath"));
endfunction

