// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = imsls_wathen(varargin)
    // Generates a random finite element matrix.
    //
    // Calling Sequence
    //  A = imsls_wathen(nx, ny)
    //  A = imsls_wathen(nx, ny, k)
    //
    // Parameters
    //    nx:        a 1-by-1 matrix of doubles, integer value, number of cells in the X coordinate
    //    ny:        a 1-by-1 matrix of doubles, integer value, number of cells in the Y coordinate
    //    k:         a 1-by-1 matrix of doubles, integer value (default k=0). If k=0, returns the finite element matrix F, if k=1, returns diag(diag(F)) \ F.
    //    A:         N-by-N symmetric positive definite matrix, where N = 3*NX*NY + 2*NX + 2*NY + 1.
    //
    // Description
    // Returns a random N-by-N finite element matrix
    //        where N = 3*NX*NY + 2*NX + 2*NY + 1.
    //        A is precisely the "consistent mass matrix" for a regular NX-by-NY
    //        grid of 8-node (serendipity) elements in 2 space dimensions.
    //        A is symmetric positive definite for any (positive) values of
    //        the "density", RHO(NX,NY), which is chosen randomly in this routine.
    //        In particular, if D=DIAG(DIAG(A)), then
    //              0.25 <= EIG(INV(D)*A) <= 4.5
    //        for any positive integers NX and NY and any densities RHO(NX,NY).
    //        This diagonally scaled matrix is returned by WATHEN(NX,NY,1).
    //
    // BEWARE - this is a sparse matrix and it quickly gets large!
    //
    // This function modifies the state of the random number generator associated
    // with the rand function.
    //
    // Examples
    // A = imsls_wathen( 3, 3, 0 )
    // A = imsls_wathen( 3, 3, 1 )
    //
    // Bibliography
    //     A.J.Wathen, Realistic eigenvalue bounds for the Galerkin mass matrix, IMA J. Numer. Anal., 7 (1987), pp. 449-457.
    //
    // Authors
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_wathen" , rhs , 2:3 )
    apifun_checklhs ( "imsls_wathen" , lhs , 1 )
    //
    // Get input arguments
    nx = varargin(1);
    ny = varargin(2);
    k = apifun_argindefault ( varargin , 3 , 0 );
    //
    // Check types
    apifun_checktype ( "imsls_wathen" , nx ,  "nx" , 1 , "constant")
    apifun_checktype ( "imsls_wathen" , ny ,  "ny" , 2 , "constant")
    apifun_checktype ( "imsls_wathen" , k ,  "k" , 3 , "constant")
    //
    // Check size
    apifun_checkscalar ( "imsls_wathen" , nx ,  "nx" , 1 )
    apifun_checkscalar ( "imsls_wathen" , ny ,  "ny" , 2 )
    apifun_checkscalar ( "imsls_wathen" , k ,  "k" , 3 )
    //
    // Check content
    apifun_checkgreq ( "imsls_wathen" , nx ,  "nx" , 1 , 1 )
	apifun_checkflint( "imsls_wathen" , nx ,  "nx" , 1)
    apifun_checkgreq ( "imsls_wathen" , ny ,  "ny" , 2 , 1 )
	apifun_checkflint( "imsls_wathen" , ny ,  "ny" , 2)
    apifun_checkgreq ( "imsls_wathen" , k ,  "k" , 3 , 0 )
	apifun_checkflint( "imsls_wathen" , k ,  "k" , 3)

    // begin of computations

    e1 = [6,-6,2,-8;-6,32,-6,20;2,-6,6,-6;-8,20,-6,32];
    e2 = [3,-8,2,-6;-8,16,-8,20;2,-8,3,-8;-6,20,-8,16];
    e = [e1,e2;e2',e1]/45;
    n = 3*nx*ny+2*nx+2*ny+1;
    A = zeros(n,n);

    // Backup the current type of the generator
    str=rand("info")
    rand("uniform")
    RHO = 100*rand(nx,ny);
    // Return the the previous random numbers type
    rand(str)

    for j=1:ny
        for i=1:nx

            nn(1) = 3*j*nx+2*i+2*j+1;
            nn(2) = nn(1)-1;
            nn(3) = nn(2)-1;
            nn(4) = (3*j-1)*nx+2*j+i-1;
            nn(5) = 3*(j-1)*nx+2*i+2*j-3;
            nn(6) = nn(5)+1;
            nn(7) = nn(6)+1;
            nn(8) = nn(4)+1;

            em = e*RHO(i,j);

            for krow=1:8
                for kcol=1:8
                    A(nn(krow),nn(kcol)) = A(nn(krow),nn(kcol))+em(krow,kcol);
                end
            end

        end
    end

    if k == 1 then
        A = diag(diag(A)) \ A;
    end

    //END wathen.sci
endfunction
