// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, flag, relres, iter, resvec] = mtlb_cgs(varargin)
    // Solves linear equations using Conjugate Gradient Squared Method with preconditioning.
    //
    // Calling Sequence
    //  x = mtlb_cgs(A, b)
    //  x = mtlb_cgs(A, b, tol)
    //  x = mtlb_cgs(A, b, tol, maxit)
    //  x = mtlb_cgs(A, b, tol, maxit, M1)
    //  x = mtlb_cgs(A, b, tol, maxit, M1, M2)
    //  x = mtlb_cgs(A, b, tol, maxit, M1, M2, x0)
    //  [x, flag] = mtlb_cgs(...)
    //  [x, flag, relres] = mtlb_cgs(...)
    //  [x, flag, relres, iter] = mtlb_cgs(...)
    //  [x, flag, relres, iter, resvec] = mtlb_cgs(...)
    //
    // Parameters
    //    A:        a n-ny-n full or sparse nonsymmetric matrix of doubles or function returning <literal>A*x</literal>
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    tol:      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1.e-6)
    //    maxit:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default min(n,20))
    //    M1:        a n-ny-n full or sparse matrix of doubles, the left preconditioner matrix (default eye(n,n)) or function returning <literal>M1\x</literal>
    //    M2:        a n-ny-n full or sparse matrix of doubles, the right preconditioner matrix (default eye(n,n)) or function returning <literal>M2\x</literal>
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default zeros(n,1))
    //    x:        a n-ny-1 full or sparse matrix of doubles,  solution vector
    //    flag:     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance within maxit iterations, 1: no convergence within maxit iterations
    //    relres:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed
    //    resvec:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. resvec(1) is the initial residual and resvec(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    // Description
    // Solves the linear system Ax=b using the
    // Conjugate Gradient Squared Method with preconditioning.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
	//
    // The argument A can be a function returning <literal>A*x</literal>.
    // In this case, the function A must have the header :
    //   <programlisting>
    //     y = A ( x )
    //   </programlisting>
    // where x is the current vector.
    // The A function must return y=A*x.
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument A can also be the list (funA,a1,a2,...).
    // In this case funA, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funA ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The argument M1 can be a function returning <literal>M1\x</literal>.
    // In this case, the function M1 must have the header :
    //   <programlisting>
    //     y = M1 ( x )
    //   </programlisting>
    // where x in the current vector.
    // The M1 function must return y=M1\x.
    //
    // The argument M1 can be the list (funM1,a1,a2,...).
    // In this case funM1, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funM1 ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The same feature is available for M2.
    //
    // The Conjugate Gradient Squared method is a variant of BiCG that applies the updating
    // operations for the A-sequence and the AT-sequences both to the same vectors. Ideally,
    // this would double the convergence rate, but in practice convergence may be much more
    // irregular than for BiCG. A practical advantage is that the method does not need the
    // multiplications with the transpose of the coefficient matrix.
    //
    // Examples
    // // Test with Wilkinson-21+ matrix
    // n=21;
    // E = diag(ones(n-1,1),1);
    // m = (n-1)/2;
    // A = diag(abs(-m:m)) + E + E';
    // b = sum(A,2);
    // tol = 1e-12;
    // maxit = 15;
    // M1 = diag([10:-1:1 1 1:10]);
    // [x,flag,relres,iter,resvec] = mtlb_cgs(A,b,tol,maxit,M1)
    // xe = ones(n,1);
    //
    // // Using cgs with a callback
    // n = 21;
    // tol = 1e-12;  maxit = 15;
    // function y = afun(x)
    //     y = [0; x(1:n-1)] + ...
    //           [((n-1)/2:-1:0)'; (1:(n-1)/2)'].*x + ...
    //           [x(2:n); 0];
    // endfunction
    // function y = mfun(r)
    //     y = r ./ [((n-1)/2:-1:1)'; 1; (1:(n-1)/2)'];
    // endfunction
    // b = afun(ones(n,1));
    // [x,flag,relres,iter,resvec] = mtlb_cgs(afun,b,tol,maxit,mfun)
    // xe = ones(n,1);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//cgs.m
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "mtlb_cgs" , rhs , 2:7 )
    apifun_checklhs ( "mtlb_cgs" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    tol = apifun_argindefault(varargin , 3 , 1.e-6 )
    maxit = apifun_argindefault(varargin , 4 , min(n,20) )
    M1 = apifun_argindefault(varargin , 5 , [] );
    M2 = apifun_argindefault(varargin , 6 , [] );
    x0 = apifun_argindefault(varargin , 7 , zeros(n,1) );
    //
    // Check types
    apifun_checktype ( "mtlb_cgs" , A ,  "A" , 1 , ["constant" "sparse" "function" "list"])
    if ( typeof(A)=="list" ) then
        apifun_checktype ( "mtlb_cgs" , A(1) ,  "A(1)" , 1 , "function" )
    end
    apifun_checktype ( "mtlb_cgs" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "mtlb_cgs" , tol ,    "tol" , 3 , "constant")
    apifun_checktype ( "mtlb_cgs" , maxit , "maxit" , 4 , "constant")
    apifun_checktype ( "mtlb_cgs" , M1 ,  "M1" , 5 , ["constant" "sparse" "function" "list"])
    if ( typeof(M1)=="list" ) then
        apifun_checktype ( "mtlb_cgs" , M1(1) ,  "M1(1)" , 5 , "function" )
    end
    apifun_checktype ( "mtlb_cgs" , M2 ,  "M2" , 6 , ["constant" "sparse" "function" "list"])
    if ( typeof(M2)=="list" ) then
        apifun_checktype ( "mtlb_cgs" , M2(1) ,  "M2(1)" , 6 , "function" )
    end
    apifun_checktype ( "mtlb_cgs" , x0 , "x0" , 7 , ["constant" "sparse"])
    //
    // Check size
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        apifun_checksquare ( "mtlb_cgs" , A , "A" , 1 )
    end
    apifun_checkveccol ( "mtlb_cgs" , b , "b" , 2 , size(b,"*") )
    apifun_checkscalar ( "mtlb_cgs" , tol , "tol" , 3 )
    apifun_checkscalar ( "mtlb_cgs" , maxit , "maxit" , 4 )
    if ( or ( typeof(M1) == ["constant" "sparse"]) ) then
        if ( M1<> [] ) then
            apifun_checkdims ( "mtlb_cgs" , M1 , "M1" , 5 , [n n] )
        end
    end
    if ( or ( typeof(M2) == ["constant" "sparse"]) ) then
        if ( M2<> [] ) then
            apifun_checkdims ( "mtlb_cgs" , M2 , "M2" , 6 , [n n] )
        end
    end
    apifun_checkveccol ( "mtlb_cgs" , x0 , "x0" , 7 , size(x0,"*") )
    //
    // Check content
    apifun_checkgreq ( "mtlb_cgs" , tol , "tol" , 3 , number_properties("tiny") )
    apifun_checkgreq ( "mtlb_cgs" , maxit , "maxit" , 4 , 1 )
	apifun_checkflint( "mtlb_cgs" , maxit , "maxit" , 4)
    //
    [x, relres, iter, flag, resvec] = imsls_cgs(A, b, x0, M1, M2, maxit, tol)
endfunction
