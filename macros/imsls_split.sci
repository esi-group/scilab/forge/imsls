// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ M, N, b ] = imsls_split( varargin )
    // Sets up the Arix splitting for Jacobi and SOR.
    //
    // Calling Sequence
    // [ M, N, b ] = imsls_split( A, b)
    // [ M, N, b ] = imsls_split( A, b, w)
    // [ M, N, b ] = imsls_split( A, b, w, flag )
    //
    // Parameters
    //    A:        a n-ny-n full or sparse  Arix of doubles
    //    b:        a n-ny-1 full or sparse  Arix of doubles, the right hand side vector (for SOR)
    //    w:        a 1-ny-1 full Arix of doubles, the relaxation parameter (default w=0.4)
    //    flag:     a 1-ny-1 full Arix of doubles, integer value, the flag for splitting method (default flag=1). If flag == 1, then splits for jacobi, if flag==2, then splits for SOR.
    //    M:        a n-ny-n full or sparse  Arix of doubles
    //    N:        a n-ny-n full or sparse  Arix of doubles such that A = M - N
    //    b:        a n-ny-1 full or sparse  Arix of doubles, the rhs vector. If flag==2, then b is updated. If flag==1, then b is unchanged by this function.
    //
    // Description
    // Sets up the Arix splitting for the stationary
    // iterative methods: jacobi and sor (gauss-seidel when w = 1.0 )
    //
    // Examples
    // // Splits for Jacobi
    // A=imsls_makefish(4);
    // b = zeros(16,1);
    // w = 1.0;
    // flag = 1;
    // [ M, N ] = imsls_split( A , b, w, flag );
    // and(A==M-N)
    //
    // // Splits for SOR
    // A=imsls_makefish(4);
    // b = zeros(16,1);
    // w = 1.0;
    // flag = 2;
    // [ M, N, b ] = imsls_split( A , b, w, flag );
    // and(A==M-N)
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/Alab//gmres.m
    //
    // Authors
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_split" , rhs , 2:4 )
    apifun_checklhs ( "imsls_split" , lhs , 0:3 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    w = apifun_argindefault(varargin , 3 , 0.4 );
    flag = apifun_argindefault(varargin , 4 , 1 )
    //
    // Check types
    apifun_checktype ( "imsls_split" , A ,  "A" , 1 , ["constant" "sparse"])
    apifun_checktype ( "imsls_split" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_split" , w , "w" , 3 , "constant")
    apifun_checktype ( "imsls_split" , flag , "flag" , 4 , "constant")
    //
    // Check size
    apifun_checksquare ( "imsls_split" , A , "A" , 1 )
    apifun_checkveccol ( "imsls_split" , b , "b" , 2 , size(b,"*") )
    apifun_checkscalar ( "imsls_split" , w , "w" , 3 )
    apifun_checkscalar ( "imsls_split" , flag , "flag" , 4 )
    //
    // Check content
    apifun_checkgreq ( "imsls_split" , w , "w" , 3 , 0 )
    apifun_checkoption ( "imsls_split" , flag , "flag" , 4 , [1 2] )

    // begin of computations

    [m,n] = size( A );

    if ( flag == 1 ),                   // jacobi splitting

        M = diag(diag(A));
        N = diag(diag(A)) - A;

    elseif ( flag == 2 ),               // sor/gauss-seidel splitting

        b = w * b;
        M =  w * tril( A, -1 ) + diag(diag( A ));
        N = -w * triu( A,  1 ) + ( 1.0 - w ) * diag(diag( A ));

    end;

endfunction
