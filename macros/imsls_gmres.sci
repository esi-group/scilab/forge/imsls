// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) XXXX - 2008 - INRIA
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, err, iter, flag, res] = imsls_gmres( varargin)
    // Solves linear equations using Generalized Minimal residual with restarts .
    //
    // Calling Sequence
    // x = imsls_gmres( A, b )
    // x = imsls_gmres( A, b, x0 )
    // x = imsls_gmres( A, b, x0, M1)
    // x = imsls_gmres( A, b, x0, M1, M2)
    // x = imsls_gmres( A, b, x0, M1, M2, restrt)
    // x = imsls_gmres( A, b, x0, M1, M2, restrt, max_it)
    // x = imsls_gmres( A, b, x0, M1, M2, restrt, max_it, tol)
    // [x, err] = imsls_gmres( ... )
    // [x, err, iter] = imsls_gmres( ... )
    // [x, err, iter, flag] = imsls_gmres( ... )
    // [x, err, iter, flag, res] = imsls_gmres( ... )
    //
    // Parameters
    //    A:        a n-ny-n full or sparse matrix of doubles or a function, nonsymmetric matrix or function returning <literal>A*x</literal> or a list
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))
    //    M1:        a n-ny-n full or sparse matrix of doubles or a function or a list, left preconditioner matrix (default eye(n,n)) or function returning <literal>M\x</literal>
    //    M2:       a n-ny-n full or sparse matrix of doubles or a function or a list, the right preconditioner matrix (default eye(n,n)) or function returning <literal>M2\x</literal> or a list.
    //    restrt:   a 1-ny-1 matrix of doubles, integer value, number of iterations between restarts (default: min(20,n)), i.e. the number of internal iterations.
    //    max_it:   a 1-ny-1 matrix of doubles, integer value, maximum number of external iterations (default n)
    //    tol:      a 1-ny-1 matrix of doubles, positive, relative error tolerance on x (default 1000*%eps)
    //    x:        a n-ny-1 full or sparse matrix of doubles, solution vector
    //    err:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value, number of iterations performed, as the total of external and internal iterations. The number of external iterations is iter/restrt.
    //    flag:     a 1-ny-1 matrix of doubles, integer value, flag=0: solution found to tolerance, flag=1: no convergence given max_it
    //    res:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    // Description
    // Solves the linear system Ax=b
    // using the Generalized Minimal residual ( GMRESm ) method with restarts.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
    //
    // The method uses external and internal iterations.
    // For each external iterations,
    // the method performs one or more internal iterations.
    //
    // The argument A can be a function returning <literal>A*x</literal>.
    // In this case, the function A must have the header :
    //   <programlisting>
    //     y = A ( x )
    //   </programlisting>
    // where x is the current vector.
    // The A function must return y=A*x.
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument A can also be the list (funA,a1,a2,...).
    // In this case funA, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funA ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The argument M1 can be a function returning <literal>M\x</literal>.
    // In this case, the function M1 must have the header :
    //   <programlisting>
    //     y = M1 ( x )
    //   </programlisting>
    // where x in the current vector.
    // The M1 function must return y=M1\x.
    //
    // The argument M1 can be the list (funM1,a1,a2,...).
    // In this case funM1, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funM1 ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The same features is available for M2.
    //
    // The Generalized Minimal Residual method computes a sequence of orthogonal vectors
    // (like MINRES), and combines these through a least-squares solve and update.
    // However, unlike MINRES (and CG) it requires storing the whole sequence, so that a large
    // amount of storage is needed.
    // For this reason, restarted versions of this method are used.
    // In restarted versions, computation and storage costs are limited by specifying a ﬁxed
    // number of vectors to be generated.
    // This method is useful for general nonsymmetric
    // matrices.
    //
    // Examples
    // A=imsls_makefish(4);
    // xe=(1:16)';
    // b=A*xe;
    // [x,err,iter,flag,res] = imsls_gmres(A,b)
    //
    // // With initial guess
    // x0=zeros(16,1);
    // [x,err,iter,flag,res] = imsls_gmres(A,b,x0);
    //
    // // With preconditionners
    // M1=eye(16,16);
    // M2=eye(16,16);
    // max_it=16;
    // tol=1000*%eps;
    // rstr=20;
    // [x,err,iter,flag,res] = imsls_gmres(A,b,x0,M1,M2,rstr,max_it,tol);
    //
    // function y=precondM1(x)
    //     y=eye(16,16)\x
    // endfunction
    // function y=precondM2(x)
    //     y=eye(16,16)\x
    // endfunction
    // function y=matvec(x)
    //     y=imsls_makefish(4)*x
    // endfunction
    //
    // [x,err,iter,flag,res] = imsls_gmres(matvec,b,x0,precondM1,precondM2,rstr,max_it,tol);
    //
    // [x,err,iter,flag,res] = imsls_gmres(A,b,x0,precondM1);
    // [x,err,iter,flag,res] = imsls_gmres(matvec,b,x0,M1);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//gmres.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin


    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_gmres" , rhs , 2:8 )
    apifun_checklhs ( "imsls_gmres" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    x0 = apifun_argindefault(varargin , 3 , zeros(n,1) );
    M1 = apifun_argindefault(varargin , 4 , [] );
    M2 = apifun_argindefault(varargin , 5 , [] );
    restrt = apifun_argindefault(varargin , 6 , 20 );
    max_it = apifun_argindefault(varargin , 7 , n )
    tol = apifun_argindefault(varargin , 8 , 1000*%eps )
    //
    // Check types
    apifun_checktype ( "imsls_gmres" , A ,  "A" , 1 , ["constant" "sparse" "function" "list"])
    if ( typeof(A)=="list" ) then
        apifun_checktype ( "imsls_gmres" , A(1) ,  "A(1)" , 1 , "function" )
    end
    apifun_checktype ( "imsls_gmres" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_gmres" , x0 , "x0" , 3 , ["constant" "sparse"])
    apifun_checktype ( "imsls_gmres" , M1 ,  "M1" , 4 , ["constant" "sparse" "function" "list"])
    if ( typeof(M1)=="list" ) then
        apifun_checktype ( "imsls_gmres" , M1(1) ,  "M1(1)" , 4 , "function" )
    end
    apifun_checktype ( "imsls_gmres" , M2 ,  "M2" , 5 , ["constant" "sparse" "function" "list"])
    if ( typeof(M2)=="list" ) then
        apifun_checktype ( "imsls_gmres" , M2(1) ,  "M2(1)" , 5 , "function" )
    end
    apifun_checktype ( "imsls_gmres" , restrt , "restrt" , 6 , "constant")
    apifun_checktype ( "imsls_gmres" , max_it , "max_it" , 7 , "constant")
    apifun_checktype ( "imsls_gmres" , tol ,    "tol" , 8 , "constant")
    //
    // Check size
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        apifun_checksquare ( "imsls_gmres" , A , "A" , 1 )
    end
    apifun_checkveccol ( "imsls_gmres" , b , "b" , 2 , size(b,"*") )
    apifun_checkveccol ( "imsls_gmres" , x0 , "x0" , 3 , size(x0,"*") )
    if ( or ( typeof(M1) == ["constant" "sparse"]) ) then
        if ( M1<> [] ) then
            apifun_checkdims ( "imsls_gmres" , M1 , "M1" , 4 , [n n] )
        end
    end
    if ( or ( typeof(M2) == ["constant" "sparse"]) ) then
        if ( M2<> [] ) then
            apifun_checkdims ( "imsls_gmres" , M2 , "M2" , 5 , [n n] )
        end
    end
    apifun_checkscalar ( "imsls_gmres" , restrt , "restrt" , 6 )
    apifun_checkscalar ( "imsls_gmres" , max_it , "max_it" , 7 )
    apifun_checkscalar ( "imsls_gmres" , tol , "tol" , 8 )
    //
    // Check content
    apifun_checkgreq ( "imsls_gmres" , restrt , "restrt" , 6 , 1 )
    apifun_checkflint( "imsls_gmres" , restrt , "restrt" , 6)
    apifun_checkgreq ( "imsls_gmres" , max_it , "max_it" , 7 , 1 )
    apifun_checkflint( "imsls_gmres" , max_it , "max_it" , 7)
    apifun_checkgreq ( "imsls_gmres" , tol , "tol" , 8 , number_properties("tiny") )
    //
    // Setup the Matrix-Vector product A into a couple (function,list-of-args).
    [myA_fun,myA_args] = imsls_setupMatvec(A,imsls_matvec);
    //
    // Setup the Left Preconditionner M1
    [myM1_fun,myM1_args] = imsls_setupPrecond(M1,imsls_precondfake,imsls_precond);
    //
    // Setup the Right Preconditionner M2
    [myM2_fun,myM2_args] = imsls_setupPrecond(M2,imsls_precondfake,imsls_precond);

    // initialization
    j = 0;
    iter = 0;
    flag = 0;
    it2 = 0;
    x = x0;

    bnrm2 = norm( b );
    if  ( bnrm2 == 0.0 ) then
        bnrm2 = 1.0;
    end

    //   r = M2 \ ( M1 \ ( b-A*x ));
    Ax = myA_fun(x,myA_args(1:$));
    r = myM1_fun(b-Ax,myM1_args(1:$));
    r = myM2_fun(r,myM2_args(1:$));

    err = norm( r ) / bnrm2;

    res = err;
    if ( err < tol ) then
        iter=0;
        return;
    end

    //   [n,n] = size(A);                             // initialize workspace

    n = size(b,1);
    m = restrt;
    V(1:n,1:m+1) = zeros(n,m+1);
    H(1:m+1,1:m) = zeros(m+1,m);
    cs(1:m) = zeros(m,1);
    sn(1:m) = zeros(m,1);
    e1    = zeros(n,1);
    e1(1) = 1.0;

    for j = 1:max_it                               // begin iteration

        //    r = M2 \ (M1 \ ( b-A*x ));
    Ax = myA_fun(x,myA_args(1:$));
        r = myM1_fun(b-Ax,myM1_args(1:$));
        r = myM2_fun(r,myM2_args(1:$));

        V(:,1) = r / norm( r );
        s = norm( r )*e1;

        for i = 1:m                                    // construct orthonormal

            it2 = it2 + 1;

            //   w = M \ (A*V(:,i));                         // basis using Gram-Schmidt
        AV = myA_fun(V(:,i),myA_args(1:$));
            w = myM1_fun(AV,myM1_args(1:$));
            w = myM2_fun(w,myM2_args(1:$));
            for k = 1:i
                H(k,i)= w'*V(:,k);
                w = w - H(k,i)*V(:,k);
            end
            H(i+1,i) = norm( w );
            V(:,i+1) = w / H(i+1,i);
            for k = 1:i-1                               // apply Givens rotation
                temp     =  cs(k)*H(k,i) + sn(k)*H(k+1,i);
                H(k+1,i) = -sn(k)*H(k,i) + cs(k)*H(k+1,i);
                H(k,i)   = temp;
            end
            [tp1,tp2] = rotmat( H(i,i), H(i+1,i) ); // form i-th rotation matrix
            cs(i)  = tp1;
            sn(i)  = tp2;
            temp   = cs(i)*s(i);                        // approximate residual norm
            s(i+1) = -sn(i)*s(i);
            s(i)   = temp;
            H(i,i) = cs(i)*H(i,i) + sn(i)*H(i+1,i);
            H(i+1,i) = 0.0;
            err  = abs(s(i+1)) / bnrm2;
            res = [res;err];
            if ( err <= tol ) then                        // update approximation
                y = H(1:i,1:i) \ s(1:i);                 // and exit
                x = x + V(:,1:i)*y;
                break;
            end

        end

        if ( err <= tol ) then
            iter=j-1+it2;
            break;
        end
        y = H(1:m,1:m) \ s(1:m);
        x = x + V(:,1:m)*y;                            // update approximation
        //    r = M \ ( b-A*x )                              // compute residual
    Ax = myA_fun(x,myA_args(1:$))
        r = myM1_fun(b-Ax,myM1_args(1:$));
        r = myM2_fun(r,myM2_args(1:$));

        s(j+1) = norm(r);
        err = s(j+1) / bnrm2;                        // check convergence
        res = [res;err];

        if ( err <= tol ) then
            iter=j+it2;
            break;
        end
        if ( j== max_it ) then
            iter=j+it2;
        end

    end

    if ( err > tol ) then
        flag = 1;
    end                 // converged

    if ( flag <> 0 ) then
        warning(msprintf(gettext("%s: Algorithm has not converged."),"imsls_gmres"))
    end
endfunction

function [ c, s ] = rotmat( a, b )

    //
    // Compute the Givens rotation matrix parameters for a and b.
    //

    [lhs,rhs]=argn(0);

    if ( rhs <> 2 ) then
        error(msprintf(gettext("%s: Wrong number of input arguments: %d to %d expected.\n"),"rotmat",2,2));
    end
    if ( size(a,1) ~= 1 | size(a,2) ~= 1 ) then
        error(msprintf(gettext("%s: Wrong size for input argument #%d: Scalar expected.\n"),"rotmat",1));
    end
    if ( size(b,1) ~= 1 | size(b,2) ~= 1 ) then
        error(msprintf(gettext("%s: Wrong size for input argument #%d: Scalar expected.\n"),"rotmat",2));
    end

    // begin of computations

    if ( b == 0.0 ) then
        c = 1.0;
        s = 0.0;
    elseif ( abs(b) > abs(a) ) then
        temp = a / b;
        s = 1.0 / sqrt( 1.0 + temp^2 );
        c = temp * s;
    else
        temp = b / a;
        c = 1.0 / sqrt( 1.0 + temp^2 );
        s = temp * c;
    end

endfunction

