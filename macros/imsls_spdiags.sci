// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2009 - DIGITEO - Yann Collette
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout=imsls_spdiags(varargin)
    // Extract and create sparse band and diagonal matrices
    //
    // Calling Sequence
    // B = imsls_spdiags(A)
    // [B,d] = imsls_spdiags(A)
    // B = imsls_spdiags(A,d)
    // A = imsls_spdiags(B,d,A)
    // A = imsls_spdiags(B,d,m,n)
    //
    // Parameters
    //    A: a m-by-n matrix of doubles, full or sparse, its nonzero or specified elements located on p diagonals.
    //    B: a Bm-by-p matrix of doubles, full or sparse, its columns are the diagonals of A. We must have Bm >= min(m,n).
    //    d: a p-by-1 matrix of doubles, full, its integer components specify the diagonals in A.
    //
    // Description
    // B = imsls_spdiags(A) extracts all nonzero diagonals from the m-by-n matrix A.
    // B is a min(m,n)-by-p matrix whose columns are the p nonzero diagonals of A.
    //
    // [B,d] = imsls_spdiags(A) returns a vector d of length p, whose integer
    // components specify the diagonals in A.
    //
    // B = imsls_spdiags(A,d) extracts the diagonals specified by d.
    //
    // A = imsls_spdiags(B,d,A) replaces the diagonals specified by d with the columns of B.
    // The output is sparse.
    //
    // A = imsls_spdiags(B,d,m,n) creates an m-by-n sparse matrix by taking the columns of B
    // and placing them along the diagonals specified by d.
    //
    // If a column of B is longer than the diagonal it is replacing, and m >= n,
    // spdiags takes elements of super-diagonals from the lower part of the column of B,
    // and elements of sub-diagonals from the upper part of the column of B.
    // However, if m < n , then super-diagonals are from the upper part of the column of B,
    // and sub-diagonals from the lower part.
    // See Example 5A and Example 5B, below.
    //
    // Examples
    // A = imsls_spdiags(matrix(1:12, 4, 3), [-1 0 1], 5, 4)
    // full(A)
    // Expected = [
    //         5 10  0  0
    //         1  6 11  0
    //         0  2  7 12
    //         0  0  3  8
    //         0  0  0  4
    // ];
    //
    // // Test 1 (m<n) - full
    // A = [
    //      0 5 0 10  0  0
    //      0 0 6  0 11  0
    //      3 0 0  7  0 12
    //      1 4 0  0  8  0
    //      0 2 5  0  0  9
    // ];
    // [B, d] = imsls_spdiags(A);
    // B_ref = [
    //          0 0 5 10
    //          0 0 6 11
    //          0 3 7 12
    //          1 4 8  0
    //          2 5 9  0
    // ];
    // d_ref = [-3 -2 1 3]';
    //
    // // Test 1 (m>n) - full
    // A = [
    //     0    0    3    1    0
    //     5    0    0    4    2
    //     0    6    0    0    5
    //    10    0    7    0    0
    //     0   11    0    8    0
    //     0    0   12    0    9
    // ];
    // [B, d] = imsls_spdiags(A);
    // B_ref = [
    //    10    5    0    0
    //    11    6    0    0
    //    12    7    3    0
    //     0    8    4    1
    //     0    9    5    2
    // ];
    // d_ref = [-3 -1 2 3]';
    //
    // // Test 2
    // // generates a sparse tridiagonal matrix for the
    // // second difference operator on n points.
    // n = 10;
    // e = ones(n,1);
    // A = imsls_spdiags([e -2*e e], -1:1, n, n);
    // full(A)
    // A_ref = [
    //   -2   1   0   0   0   0   0   0   0   0
    //    1  -2   1   0   0   0   0   0   0   0
    //    0   1  -2   1   0   0   0   0   0   0
    //    0   0   1  -2   1   0   0   0   0   0
    //    0   0   0   1  -2   1   0   0   0   0
    //    0   0   0   0   1  -2   1   0   0   0
    //    0   0   0   0   0   1  -2   1   0   0
    //    0   0   0   0   0   0   1  -2   1   0
    //    0   0   0   0   0   0   0   1  -2   1
    //    0   0   0   0   0   0   0   0   1  -2
    //    ];
    // // Turn it into Wilkinson's test matrix
    // A = imsls_spdiags(abs(-(n-1)/2:(n-1)/2)',0,A);
    // full(A)
    // A_ref = [
    //    4.5   1    0    0    0    0    0    0    0    0
    //    1   3.5    1    0    0    0    0    0    0    0
    //    0     1  2.5    1    0    0    0    0    0    0
    //    0     0    1  1.5    1    0    0    0    0    0
    //    0     0    0    1  0.5    1    0    0    0    0
    //    0     0    0    0    1  0.5    1    0    0    0
    //    0     0    0    0    0    1  1.5    1    0    0
    //    0     0    0    0    0    0    1  2.5    1    0
    //    0     0    0    0    0    0    0    1  3.5    1
    //    0     0    0    0    0    0    0    0    1  4.5
    //    ];
    // // Get the three diagonals
    // B = imsls_spdiags(A);
    // B_ref = [
    //    1   4.5   0
    //    1   3.5   1
    //    1   2.5   1
    //    1   1.5   1
    //    1   0.5   1
    //    1   0.5   1
    //    1   1.5   1
    //    1   2.5   1
    //    1   3.5   1
    //    0   4.5   1
    //    ];
    //
    // // Test 3
    // A = [11    0   13    0
    //       0   22    0   24
    //       0    0   33    0
    //      41    0    0   44
    //       0   52    0    0
    //       0    0   63    0
    //       0    0    0   74];
    // [B,d] = imsls_spdiags(A);
    // d_ref = [-3 0 2]';
    // B_ref = [41 11  0
    //          52 22  0
    //          63 33 13
    //          74 44 24];
    //
    // // Test 4
    // B = [
    //     1  1  1  1  1  1  1
    //     2  2  2  2  2  2  2
    //     3  3  3  3  3  3  3
    //     4  4  4  4  4  4  4
    //     5  5  5  5  5  5  5
    //     6  6  6  6  6  6  6
    // ];
    // d = [-4 -2 -1 0 3 4 5]';
    // A = imsls_spdiags(B,d,6,6);
    // full(A)
    // A_ref = [
    //          1 0 0 4 5 6
    //          1 2 0 0 5 6
    //          1 2 3 0 0 6
    //          0 2 3 4 0 0
    //          1 0 3 4 5 0
    //          0 2 0 4 5 6
    // ];
    //
    // // Example 5A
    // B = [
    //    1    6   11
    //    2    7   12
    //    3    8   13
    //    4    9   14
    //    5   10   15
    // ];
    //
    // // Part 1 — m is equal to n.
    // A = imsls_spdiags(B, [-2 0 2], 5, 5);
    // full(A)
    // A_ref = [
    // 6    0   13    0    0
    // 0    7    0   14    0
    // 1    0    8    0   15
    // 0    2    0    9    0
    // 0    0    3    0   10
    // ];
    // // Part 2 — m is greater than n.
    // A = imsls_spdiags(B, [-2 0 2], 5, 4);
    // full(A)
    // A_ref = [
    // 6    0   13    0
    // 0    7    0   14
    // 1    0    8    0
    // 0    2    0    9
    // 0    0    3    0
    // ];
    // // Part 3 — m is less than n.
    // A = imsls_spdiags(B, [-2 0 2], 4, 5);
    // full(A)
    // A_ref = [
    // 6    0   11    0    0
    // 0    7    0   12    0
    // 3    0    8    0   13
    // 0    4    0    9    0
    // ];
    //
    // // Example 5B
    // // Part 1.
    // A = [
    // 6    0   13    0    0
    // 0    7    0   14    0
    // 1    0    8    0   15
    // 0    2    0    9    0
    // 0    0    3    0   10
    // ];
    // B_ref = [
    // 1   6   0
    // 2   7   0
    // 3   8  13
    // 0   9  14
    // 0  10  15
    // ];
    // B = imsls_spdiags(A)
    // // Part 2.
    // A = [
    // 6    0   13    0
    // 0    7    0   14
    // 1    0    8    0
    // 0    2    0    9
    // 0    0    3    0
    // ];
    // B_ref = [
    // 1   6   0
    // 2   7   0
    // 3   8  13
    // 0   9  14
    // ];
    // B = imsls_spdiags(A)
    // // Part 3.
    // A = [
    // 6    0   11    0    0
    // 0    7    0   12    0
    // 3    0    8    0   13
    // 0    4    0    9    0
    // ];
    // B_ref = [
    // 0   6  11
    // 0   7  12
    // 3   8  13
    // 4   9   0
    // ];
    // B = imsls_spdiags(A)
    //
    // Authors
    // Copyright (C) 2009 - DIGITEO - Yann Collette
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_spdiags" , rhs , 1:4 )
    apifun_checklhs ( "imsls_spdiags" , lhs , 1:2 )

    if ( rhs == 1 ) then
        apifun_checklhs ( "imsls_spdiags" , lhs , 1:2 )
        A = varargin(1)
        //
        // Check types
        apifun_checktype ( "imsls_spdiags" , A ,  "A" , 1 , ["constant" "sparse"])
        //
        [B,d] = spdiagsBD(A)
        varargout(1) = B
        varargout(2) = d
    elseif ( rhs == 2 ) then
        apifun_checklhs ( "imsls_spdiags" , lhs , 1 )
        A = varargin(1)
        d = varargin(2)
        //
        // Check types
        apifun_checktype ( "imsls_spdiags" , A ,  "A" , 1 , ["constant" "sparse"])
        apifun_checktype ( "imsls_spdiags" , d ,  "d" , 2 , "constant")
		//
		// Check content
	    apifun_checkflint( "imsls_spdiags" , d ,  "d" , 2)
        //
        B = spdiagsAd(A,d)
        varargout(1) = B
    elseif ( rhs == 3 ) then
        apifun_checklhs ( "imsls_spdiags" , lhs , 1 )
        B = varargin(1)
        d = varargin(2)
        A = varargin(3)
        //
        // Check types
        apifun_checktype ( "imsls_spdiags" , B ,  "B" , 1 , ["constant" "sparse"])
        apifun_checktype ( "imsls_spdiags" , d ,  "d" , 2 , "constant")
        apifun_checktype ( "imsls_spdiags" , A ,  "A" , 3 , ["constant" "sparse"])
        //
        // Check size
        apifun_checkvector ( "imsls_spdiags" , d , "d" , 2 )
        //
        // Check consistency of the size of B
        [m,n] = size(A)
        p = size(d,"*")
        [Bm,Bn]=size(B)
        if ( Bm < min(m,n) ) then
            error(msprintf(gettext("%s: Wrong size of argument #%d: At least %d rows expected."),"imsls_spdiags",1,min(m,n)))
        end
        apifun_checkdims( "imsls_spdiags" , B , "B" , 1 , [Bm p] )
		//
		// Check content
	    apifun_checkflint( "imsls_spdiags" , d ,  "d" , 2)
        //
        A = spdiagsBdA(B,d,A)
        varargout(1) = A
    elseif ( rhs == 4 ) then
        apifun_checklhs ( "imsls_spdiags" , lhs , 1 )
        B = varargin(1)
        d = varargin(2)
        m = varargin(3)
        n = varargin(4)
        //
        // Check types
        apifun_checktype ( "imsls_spdiags" , B ,  "B" , 1 , ["constant" "sparse"])
        apifun_checktype ( "imsls_spdiags" , d ,  "d" , 2 , "constant")
        apifun_checktype ( "imsls_spdiags" , m ,  "m" , 3 , "constant")
        apifun_checktype ( "imsls_spdiags" , n ,  "n" , 4 , "constant")
        //
        // Check size
        apifun_checkscalar ( "imsls_spdiags" , m , "m" , 3 )
        apifun_checkscalar ( "imsls_spdiags" , n , "n" , 4 )
        //
        // Check content
	    apifun_checkflint( "imsls_spdiags" , d ,  "d" , 2)
        apifun_checkgreq ( "imsls_spdiags" , m ,  "m" , 3 , 1 )
	    apifun_checkflint( "imsls_spdiags" , m ,  "m" , 3)
        apifun_checkgreq ( "imsls_spdiags" , n ,  "n" , 4 , 1 )
	    apifun_checkflint( "imsls_spdiags" , n ,  "n" , 4)
        //
        // Check consistency of the size of B
        p = size(d,"*")
        [Bm,Bn]=size(B)
        if ( Bm < min(m,n) ) then
            error(msprintf(gettext("%s: Wrong size of argument #%d: At least %d rows expected."),"imsls_spdiags",1,min(m,n)))
        end
        apifun_checkdims( "imsls_spdiags" , B , "B" , 1 , [Bm p] )
        //
        A = spdiagsBdmn(B,d,m,n)
        varargout(1) = A
    end

endfunction

function [B, d]=spdiagsBD(A)
    [m,n]=size(A)
    nzk = 0
    // The maximum number of values in a diagonal
    ndiag = min(m,n)
    for k = -m+1:n-1
        //mprintf("k=%d\n",k)
        dk = diag(A,k);
        //disp(dk')
        if (or(dk<>0)) then
            // The nzk-th diagonal is nonzero
            nzk = nzk + 1;
            d(nzk) = k;
            // Store the values into the nzk-th column of B
            ldk = size(dk,"*");
            if (k<0) then
                if ( m<n ) then
                  istart = -k+1;
                else
                  istart = 1;
                end
            else
                if ( m<n ) then
                  istart = 1;
                else
                  istart = k+1;
                end
            end
            istop = istart+ldk-1;
            //mprintf("istart=%d, istop=%d\n",istart,istop)
            B(istart:istop,nzk)=dk;
        end
    end
endfunction

function A = spdiagsBdmn(B,d,m,n)
    A = spzeros(m,n)
    A = spdiagsBdA(B,d,A)
endfunction

function A = spdiagsBdA(B,d,A)
    [m,n] = size(A)
    // The maximum number of values in a diagonal
    ndiag = min(m,n)
    ld = size(d,"*")
    // Loop over the diagonals
    for nzk = 1 : ld
        k = d(nzk)
        //disp(k)
        if (k<0) then
            if ( m>=n ) then
                istart = 1-k
                istop = m
                jstart = 1
                jstop = k+m
                bstart = 1
                bstop = k+m
            else
                istart = 1-k
                istop = m
                jstart = 1
                jstop = m+k
                bstart = -k+1
                bstop = m
            end
        elseif (k==0) then
            istart = 1
            istop = ndiag
            jstart = 1
            jstop = ndiag
            bstart = 1
            bstop = ndiag
        else
            if ( m>=n ) then
                istart = 1
                istop = n-k
                jstart = 1+k
                jstop = n
                bstart = 1+k
                bstop = n
            else
                istart = 1
                istop = n-k
                jstart = 1+k
                jstop = n
                bstart = 1
                bstop = n-k
            end
        end
        ind = sub2ind([m,n],istart:istop,jstart:jstop)
        ldk = size(ind,"*")
        dk = B(bstart:bstop,nzk)
        A(ind) = dk
    end
endfunction


