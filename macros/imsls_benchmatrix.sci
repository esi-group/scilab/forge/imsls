// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function imsls_benchmatrix(varargin)
    // Test a matrix against all solvers.
    //
    // Calling Sequence
    //  imsls_benchmatrix(A,b)
    //  imsls_benchmatrix(A,b,x0)
    //  imsls_benchmatrix(A,b,x0,M)
    //  imsls_benchmatrix(A,b,x0,M,M1)
    //  imsls_benchmatrix(A,b,x0,M,M1,M2)
    //  imsls_benchmatrix(A,b,x0,M,M1,M2,max_it)
    //  imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol)
    //  imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol,restrt)
    //  imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol,restrt,pltmode)
    //
    // Parameters
    //    A:        a n-ny-n full or sparse matrix of doubles or function returning <literal>A*x</literal>
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))
    //    M:        a n-ny-n full or sparse matrix of doubles, preconditioner matrix (default: eye(n,n)) or function returning <literal>M\x</literal>
    //    M1:       a n-ny-n full or sparse matrix of doubles, the left preconditioner matrix (default eye(n,n)) or function returning <literal>M1\x</literal>
    //    M2:       a n-ny-n full or sparse matrix of doubles, the right preconditioner matrix (default eye(n,n)) or function returning <literal>M2\x</literal>
    //    max_it:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)
    //    tol:      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)
    //    restrt:   a 1-ny-1 matrix of doubles, integer value, number of iterations between restarts (default: min(20,n))
    //    pltmode: a 1-by-1 matrix of booleans, set to %t to create the graphics (default %f).
    //
    // Description
    // Use all the iterative methods for this matrix.
    //
    // The imsls_pcg function is used only if the matrix A is symetric.
    //
    // Any optional parameter equal to the empty matrix is set to its default value.
    //
    // The "GMRES-restart" points indicate the iterations where GMRES restarted.
    //
    // Examples
    // A = imsls_wathen(7,7,0) ;
    // mprintf("matrix: Wathen\n");
    // n=max(size(A)) ;
    // M = eye(n,n) ;
    // M1 = eye(n,n) ;
    // M2 = eye(n,n) ;
    // max_it=200 ;
    // tol=1e-6 ;
    // x0=zeros(n,1);
    // xex=ones(n,1);
    // b=A*xex ;
    // restrt=50 ;
    // imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol,restrt);
    //
    // // With default parameters
    // imsls_benchmatrix(A,b);
    //
    // // With graphics
    // imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol,restrt,%t);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //
    // Authors
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_benchmatrix" , rhs , 2:10 )
    apifun_checklhs ( "imsls_benchmatrix" , lhs , 0:1 )

    // Get arguments
    A = varargin(1);
    n = size(A,"r")
    b = varargin(2);
    x0 = apifun_argindefault ( varargin , 3 , zeros(n,1) )
    M = apifun_argindefault ( varargin , 4 , eye(n,n) )
    M1 = apifun_argindefault ( varargin , 5 , eye(n,n) )
    M2 = apifun_argindefault ( varargin , 6 , eye(n,n) )
    max_it = apifun_argindefault ( varargin , 7 , n )
    tol = apifun_argindefault ( varargin , 8 , 1000*%eps )
    restrt = apifun_argindefault ( varargin , 9 , min(20,n) )
    pltmode = apifun_argindefault ( varargin , 10 , %f )
    //
    if typeof(A)==1 then
        mprintf("Full matrix\n"),
    else
        mprintf("Sparse matrix\n"),
    end
    mprintf("size: %d * %d\n",n,n)
    issym=(max(abs(A-A')) == 0);
    if issym then
        mprintf("symmetric: yes\n"),
    else
        mprintf("symmetric: no\n"),
    end
    if typeof(A)==1 then
        d=spec(A+A');
        if (min(d)*max(d)<0) then
            mprintf("positive definite: no\n"),
        else
            mprintf("positive definite: yes\n"),
        end
        Conditionnement=cond(A);
        mprintf("conditioning: %d\n",Conditionnement);
    else
        B=full(A)
        d=spec(B+B');
        if (min(d)*max(d)<0) then
            mprintf("positive definite: no\n"),
        else
            mprintf("positive definite: yes\n"),
        end
        Conditionnement=cond(B);
        mprintf("  conditioning: %d\n",Conditionnement);
    end

    mprintf("Number of iterations\n")
    //
    // GMRES
    //
    mit=floor(max_it/restrt) ;
    [x_gm, err_gmres, it_gmres, flag, r_gmres] = imsls_gmres( A, b, x0, M1, M2, restrt, mit, tol ) ;
    if ( flag == 0 )
        mprintf("  GMRES: %d\n",it_gmres)
    else
        mprintf("  GMRES: No convergence\n")
    end
    ngmres=1*it_gmres;
    res=r_gmres(it_gmres+1);
    //
    // BiCG
    //
    mit=floor(max_it/2) ;
    [x_bi, err_bicg, it_bicg, flag, r_bicg] = imsls_bicg(A, b, x0, M1, M2, mit, tol) ;
    if ( flag == 0 )
        mprintf("  BiCG: %d\n",it_bicg)
    else
        mprintf("  BiCG: No convergence\n")
    end
    nbicg=2*it_bicg;
    res=r_bicg(it_bicg+1);
    //
    // QMR
    //
    [x_qm, err_qmr, it_qmr, flag, r_qmr] = imsls_qmr( A, b, x0, M1, M2, mit, tol ) ;
    if ( flag == 0 )
        mprintf("  QMR: %d\n",it_qmr)
    else
        mprintf("  QMR: No convergence\n")
    end
    nqmr=2*it_qmr;
    res=r_qmr(it_qmr+1);
    //
    // CGS
    //
    [x_cgs, err_cgs, it_cgs, flag, r_cgs] = imsls_cgs( A, b, x0, M1, M2, mit, tol ) ;
    if ( flag == 0 )
        mprintf("  CGS: %d\n",it_cgs)
    else
        mprintf("  CGS: No convergence\n")
    end
    ncgs=2*it_cgs;
    res=r_cgs(it_cgs+1);
    //
    // BiCGSTAB
    //
    [x_bist, err_bicgstab, it_bicgstab, flag, r_bicgstab] = imsls_bicgstab(A, b, x0, M1, M2, mit, tol) ;
    if ( flag == 0 )
        mprintf("  BiCGSTAB: %d\n",it_bicgstab)
    else
        mprintf("  BiCGSTAB: No convergence\n")
    end
    nbicgstab=2*it_bicgstab;
    res=r_bicgstab(it_bicgstab+1);

    if issym then
        mit=max_it ;
        [x_cg, err_cg, it_cg, flag, r_cg] = imsls_pcg(A, b, x0, M1, M2, mit, tol) ;
        if ( flag == 0 )
            mprintf("  PCG: %d\n",it_cg)
        else
            mprintf("  PCG: No convergence\n")
        end
        ncg=it_cg;
        res=r_cg(it_cg+1);
    end
    //
    // Gather the results into the graphics
    if ( pltmode ) then
        scf();
        xtitle("Convergence of iterative algorithms","Matrix - Vector Products","Log10(normalized residual)");
        plot((1:1:ngmres+1),log10(r_gmres),"b-")
        plot((1:2:nbicg+1),log10(r_bicg),"g-")
        plot((1:2:nqmr+1),log10(r_qmr),"r-")
        plot((1:2:ncgs+1),log10(r_cgs),"c-")
        plot((1:2:nbicgstab+1),log10(r_bicgstab),"m-")
        if issym then
            plot((1:1:ncg+1),log10(r_cg),"k-")
        end

        // Points where GMRES restarted
        plot([1:restrt:it_gmres],log10(r_gmres(1:restrt:it_gmres)),"b*");
        if issym then
            legend(["GMRES","BICG","QMR","CGS","BICGSTAB","PCG","GMRES-restart"]);
        else
            legend(["GMRES","BICG","QMR","CGS","BICGSTAB","GMRES-restart"]);
        end
    end
endfunction
