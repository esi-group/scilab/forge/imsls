<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from imsls_jacobi.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="imsls_jacobi" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>imsls_jacobi</refname><refpurpose>Solves linear equations using Jacobi Method.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   x  = imsls_jacobi(A, b)
   x  = imsls_jacobi(A, b, x0)
   x  = imsls_jacobi(A, b, x0, max_it)
   x  = imsls_jacobi(A, b, x0, max_it, tol)
   [x, err]  = imsls_jacobi(...)
   [x, err, iter]  = imsls_jacobi(...)
   [x, err, iter, flag]  = imsls_jacobi(...)
   [x, err, iter, flag, res]  = imsls_jacobi(...)

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A:</term>
      <listitem><para>        a n-ny-n full or sparse nonsymmetric matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>b:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles, right hand side vector</para></listitem></varlistentry>
   <varlistentry><term>x0:</term>
      <listitem><para>       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))</para></listitem></varlistentry>
   <varlistentry><term>max_it:</term>
      <listitem><para>   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)</para></listitem></varlistentry>
   <varlistentry><term>tol:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles,  solution vector</para></listitem></varlistentry>
   <varlistentry><term>err:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.</para></listitem></varlistentry>
   <varlistentry><term>iter:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed</para></listitem></varlistentry>
   <varlistentry><term>flag:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given max_it</para></listitem></varlistentry>
   <varlistentry><term>res:</term>
      <listitem><para>      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Solves the linear system Ax=b using the Jacobi Method.
   </para>
   <para>
Any optional input argument equal to the empty matrix [] is replaced by its default value.
   </para>
   <para>
It is not possible to provide A as a function for imsls_jacobi.
This is because the solver internally splits the matrix A into M and N, then
uses these matrices directly.
   </para>
   <para>
The Jacobi method is based on solving for every variable locally with respect to the
other variables; one iteration of the method corresponds to solving for every variable
once.
The resulting method is easy to understand and implement, but convergence is
slow
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A=imsls_makefish(4);
xe=(1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_jacobi(A,b)

// With initial guess
x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_jacobi(A,b,x0);

// Convergence
// Increase max_it
// Use the default x0
max_it=200;
tol=1000*%eps;
[x,err,iter,flag,res] = imsls_jacobi(A,b,[],max_it,tol);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).</para>
   <para>http://www.netlib.org/templates/matlab//jacobi.m</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst</member>
   <member>Copyright (C) 2005 - INRIA - Sage Group (IRISA)</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
