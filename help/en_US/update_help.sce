// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
//
// Generate the functions help
helpdir = fullfile(cwd);
mprintf("Generating help in %s...\n",helpdir);
funmat = [
  "imsls_bicg"
  "imsls_bicgstab"
  "imsls_cgs"
  "imsls_cheby"
  "imsls_gmres"
  "imsls_jacobi"
  "imsls_pcg"
  "imsls_qmr"
  "imsls_sor"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the support functions help
helpdir = fullfile(cwd,"support");
mprintf("Generating help in %s...\n",helpdir);
funmat = [
  "imsls_lehmer"
  "imsls_makefish"
  "imsls_nonsym"
  "imsls_wathen"
  "imsls_split"
  "imsls_matgen"
  "imsls_tester"
  "imsls_getpath"
  "imsls_benchmatrix"
  "imsls_spdiags"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
// Generate the compatibility functions help
helpdir = fullfile(cwd,"compatibility");
mprintf("Generating help in %s...\n",helpdir);
funmat = [
  "mtlb_gmres"
  "mtlb_bicg"
  "mtlb_bicgstab"
  "mtlb_cgs"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


