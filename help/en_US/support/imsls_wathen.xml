<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from imsls_wathen.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="imsls_wathen" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>imsls_wathen</refname><refpurpose>Generates a random finite element matrix.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   A = imsls_wathen(nx, ny, k)

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>nx:</term>
      <listitem><para>        a 1-by-1 matrix of doubles, integer value, number of cells in the X coordinate</para></listitem></varlistentry>
   <varlistentry><term>ny:</term>
      <listitem><para>        a 1-by-1 matrix of doubles, integer value, number of cells in the Y coordinate</para></listitem></varlistentry>
   <varlistentry><term>k:</term>
      <listitem><para>         a 1-by-1 matrix of doubles, integer value, if k=0, returns the finite element matrix F, if k=1, returns diag(diag(F)) \ F.</para></listitem></varlistentry>
   <varlistentry><term>A:</term>
      <listitem><para>         N-by-N symmetric positive definite matrix, where N = 3*NX*NY + 2*NX + 2*NY + 1.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns a random N-by-N finite element matrix
where N = 3*NX*NY + 2*NX + 2*NY + 1.
A is precisely the "consistent mass matrix" for a regular NX-by-NY
grid of 8-node (serendipity) elements in 2 space dimensions.
A is symmetric positive definite for any (positive) values of
the "density", RHO(NX,NY), which is chosen randomly in this routine.
In particular, if D=DIAG(DIAG(A)), then
0.25 &lt;= EIG(INV(D)*A) &lt;= 4.5
for any positive integers NX and NY and any densities RHO(NX,NY).
This diagonally scaled matrix is returned by WATHEN(NX,NY,1).
   </para>
   <para>
BEWARE - this is a sparse matrix and it quickly gets large!
   </para>
   <para>
This function modifies the state of the random number generator associated
with the rand function.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A = imsls_wathen( 3, 3, 0 )
A = imsls_wathen( 3, 3, 1 )

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>A.J.Wathen, Realistic eigenvalue bounds for the Galerkin mass matrix, IMA J. Numer. Anal., 7 (1987), pp. 449-457.</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2005 - INRIA - Sage Group (IRISA)</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
