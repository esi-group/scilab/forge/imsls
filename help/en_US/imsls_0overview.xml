<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 *
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
-->
<refentry version="5.0-subset Scilab"
          xml:id="imsls_overview"
          xml:lang="fr"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 16-12-2008 $</pubdate>
  </info>

  <refnamediv>
    <refname>Overview</refname>

    <refpurpose>An overview of the Iterative Methods for Sparse Linear Systems toolbox.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Purpose</title>

    <para>
    The Imsls module provides iterative methods for sparse linear systems of equations.
    More precisely, it provides functions to find <literal>x</literal> such that <literal>A*x=b</literal>,
    where <literal>A</literal> is a n-by-n matrix of doubles and <literal>b</literal> is a n-by-1
    matrix of doubles.
    Although these functions can manage full matrices, it is mainly designed for sparse matrices.
    </para>

    <para>
One of the interesting point here is that the matrix-vector products
or the preconditionning steps <literal>M\x</literal> can be performed either with full or sparse
matrices, or with callback functions.
This flexibility makes the module convenient to use in situations when
the sparse matrices are not stored in memory, since only the matrix-vector
product (or the preconditionning step <literal>M\x</literal>) is required.
    </para>

    <para>
Moreover, we provide Matlab-compatible pcg, gmres and qmr solvers:
	<itemizedlist>
	<listitem>	<para>	the order of the arguments are the same as in Matlab,    </para>	</listitem>
	<listitem>	<para>	the default values of the Matlab functions are the same as in Matlab,    </para>	</listitem>
	<listitem>	<para>	the headers of the callback functions are the same as in Matlab.    </para>	</listitem>
	</itemizedlist>
This contrasts with Scilab's internal functions, where the two last points are
completely unsatisfied.
    </para>

    <para>
Finally, we provide a complete test suite for these functions, which
are using robust argument checking.
    </para>

  </refsection>


  <refsection>
    <title>Quick start</title>

    <para>
    The imsls_gmres function finds <literal>x</literal> such that <literal>A*x=b</literal>.
    In the following script, we create a 16-by-16 matrix of doubles.
    Although this matrix is symmetric, the <literal>imsls_gmres</literal> function can manage nonsymmetric matrices.
    Then we define the expected result <literal>xe</literal>, and compute the
    right hand side <literal>b</literal>.
    Finally, we call the <literal>imsls_gmres</literal>, which returns <literal>x</literal>.
    </para>

    <programlisting role="example"><![CDATA[
A=imsls_makefish(4);
xe=(1:16)';
b=A*xe;
x = imsls_gmres(A,b)
 ]]></programlisting>

    <para>
    The previous script produces the following output.
    </para>

    <programlisting role="example"><![CDATA[
-->x = imsls_gmres(A,b)
 x  =
    1.
    2.
    3.
    4.
    5.
    6.
    7.
    8.
    9.
   10.
    11.
    12.
    13.
    14.
    15.
    16.
 ]]></programlisting>

    <para>
    We might be interested by getting more information on the status of the
    output <literal>x</literal>.
    This is done in the following example, where we get the relative residual <literal>err</literal>,
    the number of iterations <literal>iter</literal>, the output flag <literal>flag</literal> and
    the history of the relative residuals <literal>res</literal>.
    The relative residual <literal>err</literal> is equal to <literal>norm(A*x-b)/norm(b)</literal> if <literal>norm(b)</literal> is nonzero,
    and is equal to <literal>norm(A*x-b)</literal> if <literal>norm(b)</literal> is zero.
    </para>

    <programlisting role="example"><![CDATA[
[x,err,iter,flag,res] = imsls_gmres(A,b)
 ]]></programlisting>

    <para>
    The previous script produces the following output.
    </para>

    <programlisting role="example"><![CDATA[
-->[x,err,iter,flag,res] = imsls_gmres(A,b)
 res  =
    1.
    0.4450833
    0.2656239
    0.1523145
    0.0824680
    0.0319760
    3.356D-16
 flag  =
    0.
 iter  =
    6.
 err  =
    3.356D-16
 x  =
    1.
    2.
    3.
    4.
    5.
    6.
    7.
    8.
    9.
   10.
    11.
    12.
    13.
    14.
    15.
    16.
 ]]></programlisting>

    <para>
    In the following script, we create a plot of the iterations versus the 2-norm relative residual.
    </para>

    <programlisting role="example"><![CDATA[
scf();
plot(1:iter+1,log10(res),"b*-");
xtitle("Convergence of GMRES","Iterations","Base-10 logarithm of 2-norm of relative residual")
 ]]></programlisting>

  </refsection>

  <refsection>
    <title>Choosing a method</title>
    <para>
	In the case where we are searching an iterative solver for a particular matrix,
	we may first try the GMRES method, which may provide good performances.
	Moreover, the GMRES method is applicable to nonsymmetric matrices and only requires
	matrix-vector products with the coefficient matrix..
	If GMRES does not work well, we may try Bi-CGSTAB.
    </para>

    <para>
	Some algorithms can be applied to symmetric positive definite systems, while some other
	algorithms can be applied to nonsymmetric matrices.
    </para>

    <para>
	Solvers which can be applied only on symmetric positive definite matrices are:
	<itemizedlist>
	<listitem>	<para>	imsls_sor    </para>	</listitem>
	<listitem>	<para>	imsls_pcg    </para>	</listitem>
	<listitem>	<para>	imsls_cheby    </para>	</listitem>
	</itemizedlist>
    </para>

	<para>
	Solvers which can be applied only on nonsymmetric matrices are:
	<itemizedlist>
	<listitem>	<para>	imsls_jacobi    </para>	</listitem>
	<listitem>	<para>	imsls_gmres    </para>	</listitem>
	<listitem>	<para>	imsls_bicg    </para>	</listitem>
	<listitem>	<para>	imsls_cgs    </para>	</listitem>
	<listitem>	<para>	imsls_bicgstab    </para>	</listitem>
	<listitem>	<para>	imsls_qmr    </para>	</listitem>
	</itemizedlist>
    </para>

    <para>
	For most algorithms, the matrix <literal>A</literal> or the preconditionner matrix
	<literal>M</literal> or <literal>M2</literal> can be provided as a dense or sparse
	matrix, or as a function.
	In this case, the argument <literal>A</literal> (or <literal>M</literal> or <literal>M2</literal>)
	must be a function which returns <literal>A*x</literal> (or <literal>M\x</literal> or <literal>M2\x</literal>).
	In some cases, the transpose product <literal>A'*x</literal> (or <literal>M'\x</literal> or <literal>M2'\x</literal>)
	must be returned.
    </para>

    <para>
	This feature could not be provided for all solvers.
	This is because some algorithms perform explicit computations on <literal>A</literal> (or <literal>M</literal> or <literal>M2</literal>).
    </para>

    <para>
	Solvers for which the matrix <literal>A</literal> or the preconditionner matrix
	<literal>M</literal> or <literal>M2</literal> can be provided as a function are:
	<itemizedlist>
	<listitem>	<para>	imsls_gmres    </para>	</listitem>
	<listitem>	<para>	imsls_bicg    </para>	</listitem>
	<listitem>	<para>	imsls_cgs    </para>	</listitem>
	<listitem>	<para>	imsls_bicgstab    </para>	</listitem>
	<listitem>	<para>	imsls_qmr    </para>	</listitem>
	<listitem>	<para>	imsls_pcg    </para>	</listitem>
	</itemizedlist>
	
	Solvers for which the matrix <literal>A</literal> or the preconditionner matrix
	<literal>M</literal> or <literal>M2</literal> cannot be provided as a function are:
	<itemizedlist>
	<listitem>	<para>	imsls_jacobi    </para>	</listitem>
	<listitem>	<para>	imsls_sor    </para>	</listitem>
	<listitem>	<para>	imsls_cheby    </para>	</listitem>
	</itemizedlist>
    </para>

  </refsection>

  <refsection>
    <title>In case of no convergence</title>

    <para>
    It may happen that the algorithm does not converge.
    In the following example, the pcg algorithm is used to solve a system of equations
    depending on a 16-by-16 matrix.
    </para>

    <programlisting role="example"><![CDATA[
A=imsls_lehmer(16);
xe = (1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_pcg(A,b);
 ]]></programlisting>

    <para>
    The previous script produces the following output.
    </para>

    <programlisting role="example"><![CDATA[
-->[x,err,iter,flag,res] = imsls_pcg(A,b);
WARNING: imsls_pcg: Algorithm has not converged.
 ]]></programlisting>

    <para>
    We can check that the algorithm has not converged by analyzing the
    <literal>flag</literal> variable, which is nonzero.
    We can also check that the relative residual <literal>err</literal>
    is greater than the default tolerance <literal>1000*%eps</literal>.
    </para>

    <programlisting role="example"><![CDATA[
-->flag
 flag  =
    1.
-->err
 err  =
    5.519D-08
-->1000*%eps
 ans  =
    2.220D-13
 ]]></programlisting>

    <para>
    In order to fix this problem, we can increase the tolerance
    <literal>tol</literal> (i.e. we ask for a less accurate solution <literal>x</literal>)
    or increase <literal>max_it</literal> (i.e. we let the algorithm have more iterations to
    converge).
    In the following session, we just increase the maximum number of iterations to 100.
    </para>

    <programlisting role="example"><![CDATA[
-->[x,err,iter,flag,res] = imsls_pcg(A,b,[],[],[],100);
-->flag
 flag  =
    0.
-->err
 err  =
    1.363D-14
 ]]></programlisting>

  </refsection>

  <refsection>
    <title>History of the module</title>

    <para>
	In 1993, the Book "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods"
	was published.
	Associated source codes in the Matlab language were provided at http://www.netlib.org/templates/.
	From 2000 to 2006, these function have been ported to Scilab 4 by Aladin Group (IRISA-INRIA).
	Starting in 2010, Michael Baudin upgraded this module to Scilab 5.
	I updated the management of the input arguments, the arguments checking and the
	management of the callbacks (for the matrix-vector product <literal>A*x</literal> and
	the preconditionning <literal>M\x</literal>).
	The argument management is now based on apifun, which greatly improves the robustness and
	simplifies the writing of the functions.
	I described the input arguments more precisely in the help pages, added examples and created unit tests
	(based on the assert module).
	I added the right preconditionner M2 for all functions.
	I created the Matlab compatibility functions.
    </para>
  </refsection>

  <refsection>
    <title>Authors</title>

    <para>
	1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
	</para>
    <para>
	1993 - Univ. of Tennessee and Oak Ridge National Laboratory
	</para>
    <para>
	2000 - 2001 - INRIA - Aladin Group
	</para>
    <para>
	2010 - 2011 - DIGITEO - Michael Baudin
	</para>
  </refsection>

  <refsection>
    <title>Acknowledgements</title>

    <para>
    Michael Baudin thanks Thierry Clopeau for his advices on this topic.
    </para>

  </refsection>

  <refsection>
    <title>Licence</title>

    <para>
    This toolbox is distributed under the CeCILL.
    </para>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>
	"Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
	</para>
    <para>
	"Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
	</para>
  </refsection>
</refentry>

