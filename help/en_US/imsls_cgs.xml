<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from imsls_cgs.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="imsls_cgs" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>imsls_cgs</refname><refpurpose>Solves linear equations using Conjugate Gradient Squared Method with preconditioning.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   x = imsls_cgs(A, b)
   x = imsls_cgs(A, b, x0)
   x = imsls_cgs(A, b, x0, M1)
   x = imsls_cgs(A, b, x0, M1, M2)
   x = imsls_cgs(A, b, x0, M1, M2, max_it)
   x = imsls_cgs(A, b, x0, M1, M2, max_it, tol)
   [x, err] = imsls_cgs(...)
   [x, err, iter] = imsls_cgs(...)
   [x, err, iter, flag] = imsls_cgs(...)
   [x, err, iter, flag, res] = imsls_cgs(...)

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A:</term>
      <listitem><para>        a n-ny-n full or sparse nonsymmetric matrix of doubles or function returning <literal>A*x</literal></para></listitem></varlistentry>
   <varlistentry><term>b:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles, right hand side vector</para></listitem></varlistentry>
   <varlistentry><term>x0:</term>
      <listitem><para>       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default zeros(n,1))</para></listitem></varlistentry>
   <varlistentry><term>M1:</term>
      <listitem><para>        a n-ny-n full or sparse matrix of doubles, the left preconditioner matrix (default eye(n,n)) or function returning <literal>M1\x</literal></para></listitem></varlistentry>
   <varlistentry><term>M2:</term>
      <listitem><para>        a n-ny-n full or sparse matrix of doubles, the right preconditioner matrix (default eye(n,n)) or function returning <literal>M2\x</literal></para></listitem></varlistentry>
   <varlistentry><term>max_it:</term>
      <listitem><para>   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)</para></listitem></varlistentry>
   <varlistentry><term>tol:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles,  solution vector</para></listitem></varlistentry>
   <varlistentry><term>err:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.</para></listitem></varlistentry>
   <varlistentry><term>iter:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed</para></listitem></varlistentry>
   <varlistentry><term>flag:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance within max_it iterations, 1: no convergence within max_it iterations</para></listitem></varlistentry>
   <varlistentry><term>res:</term>
      <listitem><para>      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Solves the linear system Ax=b using the
Conjugate Gradient Squared Method with preconditioning.
   </para>
   <para>
Any optional input argument equal to the empty matrix [] is replaced by its default value.
   </para>
   <para>
The argument A can be a function returning <literal>A*x</literal>.
In this case, the function A must have the header :
<programlisting>
y = A ( x )
</programlisting>
where x is the current vector.
The A function must return y=A*x.
   </para>
   <para>
It might happen that the function requires additionnal arguments to be evaluated.
In this case, we can use the following feature.
The argument A can also be the list (funA,a1,a2,...).
In this case funA, the first element in the list, must have the header:
<programlisting>
y = funA ( x , a1 , a2 , ... )
</programlisting>
where the input arguments a1, a2, ...
are automatically be appended at the end of the calling sequence.
   </para>
   <para>
The argument M1 can be a function returning <literal>M1\x</literal>.
In this case, the function M1 must have the header :
<programlisting>
y = M1 ( x )
</programlisting>
where x in the current vector.
The M1 function must return y=M1\x.
   </para>
   <para>
The argument M1 can be the list (funM1,a1,a2,...).
In this case funM1, the first element in the list, must have the header:
<programlisting>
y = funM1 ( x , a1 , a2 , ... )
</programlisting>
where the input arguments a1, a2, ...
are automatically be appended at the end of the calling sequence.
   </para>
   <para>
The same feature is available for M2.
   </para>
   <para>
The Conjugate Gradient Squared method is a variant of BiCG that applies the updating
operations for the A-sequence and the AT-sequences both to the same vectors. Ideally,
this would double the convergence rate, but in practice convergence may be much more
irregular than for BiCG. A practical advantage is that the method does not need the
multiplications with the transpose of the coefficient matrix.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A=imsls_makefish(4);
xe=(1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_cgs(A,b)

x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_cgs(A,b,x0);

M1=eye(16,16);
M2=eye(16,16);
max_it=16;
tol=1000*%eps;
[x,err,iter,flag,res] = imsls_cgs(A,b,x0,M1,M2,max_it,tol);

function y=precondM1(x)
y=2*eye(16,16)\x
endfunction
function y=matvec(x)
y=imsls_makefish(4)*x
endfunction

[x,err,iter,flag,res] = imsls_cgs(matvec,b,x0,precondM1,[],max_it,tol);

[x,err,iter,flag,res] = imsls_cgs(A,b,x0,precondM1);
[x,err,iter,flag,res] = imsls_cgs(matvec,b,x0,M1);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).</para>
   <para>http://www.netlib.org/templates/matlab//cgs.m</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst</member>
   <member>Copyright (C) 2005 - INRIA - Sage Group (IRISA)</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
