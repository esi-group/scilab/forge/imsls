<?xml version="1.0" encoding="UTF-8"?>
<!--
 *
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2008-2009 - INRIA - Michael Baudin
 * Copyright (C) XXXX-2008 - INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="mtlb_pcg" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>mtlb_pcg</refname>

    <refpurpose>Solves linear equations using Conjugate Gradient method with preconditioning.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>
      x = mtlb_pcg(A,b)
      x = mtlb_pcg(A,b,tol)
      x = mtlb_pcg(A,b,tol,maxit)
      x = mtlb_pcg(A,b,tol,maxit,M1)
      x = mtlb_pcg(A,b,tol,maxit,M1,M2)
      x = mtlb_pcg(A,b,tol,maxit,M1,M2,x0)
      [x,flag] = mtlb_pcg(A,b,...)
      [x,flag,relres] = mtlb_pcg(A,b,...)
      [x,flag,relres,iter] = mtlb_pcg(A,b,...)
      [x,flag,relres,iter,resvec] = mtlb_pcg(A,b,...)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>A</term>

        <listitem>
          <para>
            a dense or sparse n-by-n matrix, or a function, or a list computing
            <literal>A*x</literal> for each given <literal>x</literal>.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>b</term>

        <listitem>
          <para>a n-by-1 dense or sparse matrix, the right hand side vector</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>tol</term>

        <listitem>
          <para>
            a 1-by-1 matrix of doubles, positive, the error relative tolerance (default tol=1e-6).
            The termination criteria is based on the 2-norm of the
            residual <literal>r=b-Ax</literal>, divided by the 2-norm of the right hand side b.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>maxIter</term>

        <listitem>
          <para>
		  a 1-by-1 matrix of doubles, integer value, positive,
		  the maximum number of iterations (default maxIter=min(n,20)).
		  </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>M1</term>

        <listitem>
          <para>
            a n-by-n dense or sparse matrix of doubles or a function or a list,
			the left preconditioner (default M1=[]).
			If M1 is a function, it returns <literal>M1\x</literal>.
			See below for details.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>M2</term>

        <listitem>
          <para>
            a n-by-n dense or sparse matrix of doubles or a function or a list,
			the right preconditioner (default M2=[]).
			If M2 is a function, it returns <literal>M2\x</literal>.
			See below for details.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>x0</term>

        <listitem>
          <para>
		  a n-by-1 dense or sparse matrix of doubles, the initial guess (default: zeros(n,1))
		  </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>x</term>

        <listitem>
          <para>
		  a n-by-1 matrix of doubles, the solution or the equation A*x=b
		  </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>flag</term>

        <listitem>
          <para>
            a 1-by-1 matrix of doubles, integer value,
			flag=0 if <literal>mtlb_pcg</literal> converged to the desired tolerance
            within <literal>maxi</literal> iterations, flag=1 if not.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>relres</term>

        <listitem>
          <para>
		  a 1-by-1 matrix of doubles, the final relative 2-norm of the residual <literal>A*x-b</literal>.
		  This is equal to <literal>norm(A*x-b)/norm(b)</literal> if <literal>norm(b)</literal> is nonzero,
		  and is equal to <literal>norm(A*x-b)</literal> if <literal>norm(b)</literal> is zero.
		  </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>iter</term>

        <listitem>
          <para>
		  a 1-by-1 matrix of doubles, integer value, the number of iterations performed.
		  </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>resvec</term>

        <listitem>
          <para>
		  a (iter+1)-by-1 matrix of doubles, the residual relative norms.
		  resvec(1) is the initial relative residual and
		  resvec(i) is the relative residual for the iteration #i, for i=1,2,...,iter.
		  </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>
      Solves the linear system <literal>Ax=b</literal> using the conjugate
      gradient method with or without preconditioning.
    </para>

    <para>
    Any optional input argument equal to the empty matrix [] is replaced by its default value.
    </para>
	
   <para>
The argument A can be a function returning <literal>A*x</literal>.
In this case, the function A must have the header :
<programlisting>
y = A ( x )
</programlisting>
where x is the current vector and y=A*x.
   </para>
   <para>
It might happen that the function requires additionnal arguments to be evaluated.
In this case, we can use the following feature.
The argument A can also be the list (funA,a1,a2,...).
In this case funA, the first element in the list, must have the header:
<programlisting>
y = funA ( x , a1 , a2 , ... )
</programlisting>
where the input arguments a1, a2, ...
are automatically be appended at the end of the calling sequence.
   </para>
   <para>
The argument M1 can be a function returning <literal>M1\x</literal>.
In this case, the function M1 must have the header :
<programlisting>
y = M1 ( x )
</programlisting>
where x in the current vector and y=M1\x.
   </para>
   <para>
The argument M1 can be the list (funM1,a1,a2,...).
In this case funM1, the first element in the list, must have the header:
<programlisting>
y = funM1 ( x , a1 , a2 , ... )
</programlisting>
where the input arguments a1, a2, ...
are automatically be appended at the end of the calling sequence.
   </para>
   <para>
The same feature is available for M2.
   </para>

    <para>
	Any input argument equal to the empty matrix [] is replaced by its default value.
    </para>


    <para>
	   There are two differences between <literal>mtlb_pcg</literal> and <literal>imsls_pcg</literal>:
	   the order of the input and output arguments, the default values.
    </para>

  </refsection>

  <refsection>
    <title>
      Example with well conditionned and ill conditionned
      problems
    </title>

    <para>
      In the following example, two linear systems are solved. The first
      maxtrix has a condition number equals to ~0.02, which makes the algorithm
      converge in exactly 10 iterations.
	  Since this is the size of the matrix,
      it is an expected behaviour for a gradient conjugate method.
	  The second one has a large condition number roughly equals to 1.d6, which makes the algorithm
      converge in a larger 22 iterations.
	  This is why the parameter maxIter is set to 30.
    </para>

    <programlisting role="example">
      <![CDATA[
//Well conditionned problem
A=[ 94  0   0   0    0   28  0   0   32  0
     0   59  13  5    0   0   0   10  0   0
     0   13  72  34   2   0   0   0   0   65
     0   5   34  114  0   0   0   0   0   55
     0   0   2   0    70  0   28  32  12  0
     28  0   0   0    0   87  20  0   33  0
     0   0   0   0    28  20  71  39  0   0
     0   10  0   0    32  0   39  46  8   0
     32  0   0   0    12  33  0   8   82  11
     0   0   65  55   0   0   0   0   11  100];

b=ones(10,1);
[x, fail, err, iter, res]=mtlb_pcg(A,b,1d-12,15)

//Ill contionned one
A=[ 894     0   0     0   0   28  0   0   1000  70000
      0      5   13    5   0   0   0   0   0     0
      0      13  72    34  0   0   0   0   0     6500
      0      5   34    1   0   0   0   0   0     55
      0      0   0     0   70  0   28  32  12    0
      28     0   0     0   0   87  20  0   33    0
      0      0   0     0   28  20  71  39  0     0
      0      0   0     0   32  0   39  46  8     0
      1000   0   0     0   12  33  0   8   82    11
      70000  0   6500  55  0   0   0   0   11    100];

[x, fail, err, iter, res]=mtlb_pcg(A,b,1d-12,30)
 ]]>
    </programlisting>
  </refsection>

  <refsection>
    <title>
      Examples with A given as a sparse matrix, or function, or
      list
    </title>

    <para>
      The following example shows that the method can handle sparse
      matrices as well. It also shows the case where a function, computing the
      right-hand side, is given to the "mtlb_pcg" function.
	  The final case shown by this example, is when a list is passed to the function.
    </para>

    <programlisting role="example">
      <![CDATA[
//Well conditionned problem
A=[ 94  0   0   0    0   28  0   0   32  0
     0   59  13  5    0   0   0   10  0   0
     0   13  72  34   2   0   0   0   0   65
     0   5   34  114  0   0   0   0   0   55
     0   0   2   0    70  0   28  32  12  0
     28  0   0   0    0   87  20  0   33  0
     0   0   0   0    28  20  71  39  0   0
     0   10  0   0    32  0   39  46  8   0
     32  0   0   0    12  33  0   8   82  11
     0   0   65  55   0   0   0   0   11  100];
xe = (1:10)';
b = A*xe;

// Convert A into a sparse matrix
Asparse=sparse(A);
[x, fail, err, iter, res]=mtlb_pcg(Asparse,b,1d-12,30)

// Define a function which computes the right-hand side.
function y=Atimesx(x)
  A=[ 94  0   0   0    0   28  0   0   32  0
       0   59  13  5    0   0   0   10  0   0
       0   13  72  34   2   0   0   0   0   65
       0   5   34  114  0   0   0   0   0   55
       0   0   2   0    70  0   28  32  12  0
       28  0   0   0    0   87  20  0   33  0
       0   0   0   0    28  20  71  39  0   0
       0   10  0   0    32  0   39  46  8   0
       32  0   0   0    12  33  0   8   82  11
       0   0   65  55   0   0   0   0   11  100];
  y=A*x
endfunction

// Pass the script Atimesx to the primitive
[x, fail, err, iter, res]=mtlb_pcg(Atimesx,b,1d-12,30)

// Define a function which computes the right-hand side.
function y=Atimesxbis(x,A)
  y=A*x
endfunction

// Pass a list to the primitive
Alist = list(Atimesxbis,Asparse);
[x, fail, err, iter, res]=mtlb_pcg(Alist,b,1d-12,30)
 ]]>
    </programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>
    <para>Sage Group, IRISA, 2004</para>
    <para>Serge Steer, INRIA, 2006</para>
    <para>Michael Baudin, INRIA, 2008-2009</para>
    <para>Michael Baudin, DIGITEO, 2011</para>
  </refsection>

  <refsection>
    <title>References</title>

    <para>
      "Templates for the Solution of Linear Systems: Building Blocks for
      Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra,
      Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993, ftp
      netlib2.cs.utk.edu/linalg/templates.ps
    </para>

    <para>
      "Iterative Methods for Sparse Linear Systems, Second Edition", Saad,
      SIAM Publications, 2003, ftp
      ftp.cs.umn.edu/dept/users/saad/PS/all_ps.zip
    </para>
  </refsection>
</refentry>
