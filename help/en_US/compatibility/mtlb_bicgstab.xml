<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from mtlb_bicgstab.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="mtlb_bicgstab" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>mtlb_bicgstab</refname><refpurpose>Solves linear equations using BiConjugate Gradient Stabilized Method with preconditioning.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   x = mtlb_bicgstab(A, b)
   x = mtlb_bicgstab(A, b, tol)
   x = mtlb_bicgstab(A, b, tol, maxit)
   x = mtlb_bicgstab(A, b, tol, maxit, M1)
   x = mtlb_bicgstab(A, b, tol, maxit, M1, M2)
   x = mtlb_bicgstab(A, b, tol, maxit, M1, M2, x0)
   [x, flag] = mtlb_bicgstab(...)
   [x, flag, relres] = mtlb_bicgstab(...)
   [x, flag, relres, iter] = mtlb_bicgstab(...)
   [x, flag, relres, iter, resvec] = mtlb_bicgstab(...)

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A:</term>
      <listitem><para>        a n-ny-n full or sparse nonsymmetric matrix of doubles or function returning <literal>A*x</literal></para></listitem></varlistentry>
   <varlistentry><term>b:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles, right hand side vector</para></listitem></varlistentry>
   <varlistentry><term>tol:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1e-6)</para></listitem></varlistentry>
   <varlistentry><term>maxit:</term>
      <listitem><para>   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default min(n,20))</para></listitem></varlistentry>
   <varlistentry><term>M1:</term>
      <listitem><para>        a n-ny-n full or sparse matrix of doubles, left preconditioner matrix (default: eye(n,n)) or function returning <literal>M1\x</literal></para></listitem></varlistentry>
   <varlistentry><term>M2:</term>
      <listitem><para>        a n-ny-n full or sparse matrix of doubles, right preconditioner matrix (default: eye(n,n)) or function returning <literal>M2\x</literal></para></listitem></varlistentry>
   <varlistentry><term>x0:</term>
      <listitem><para>       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles,  solution vector</para></listitem></varlistentry>
   <varlistentry><term>flag:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given maxit, -1: breakdown caused by rho = 0, -2: breakdown caused by omega = 0</para></listitem></varlistentry>
   <varlistentry><term>relres:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.</para></listitem></varlistentry>
   <varlistentry><term>iter:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed</para></listitem></varlistentry>
   <varlistentry><term>resvec:</term>
      <listitem><para>      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. resvec(1) is the initial residual and resvec(i+1) is the residual for the iteration i, for i=1,2,...,iter.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Solves the linear system Ax=b using the
BiConjugate Gradient Stabilized Method with preconditioning.
   </para>
   <para>
Any optional input argument equal to the empty matrix [] is replaced by its default value.
   </para>
   <para>
The Biconjugate Gradient Stabilized method is a variant of BiCG, like CGS, but using
different updates for the AT-sequence in order to obtain smoother convergence than
CGS.
   </para>
   <para>
The argument A can be a function returning <literal>A*x</literal>.
In this case, the function A must have the header :
<programlisting>
y = A ( x )
</programlisting>
where x is the current vector.
The A function must return y=A*x.
   </para>
   <para>
It might happen that the function requires additionnal arguments to be evaluated.
In this case, we can use the following feature.
The argument A can also be the list (funA,a1,a2,...).
In this case funA, the first element in the list, must have the header:
<programlisting>
y = funA ( x , a1 , a2 , ... )
</programlisting>
where the input arguments a1, a2, ...
are automatically be appended at the end of the calling sequence.
   </para>
   <para>
The argument M1 can be a function returning <literal>M1\x</literal>.
In this case, the function M1 must have the header :
<programlisting>
y = M1 ( x )
</programlisting>
where x in the current vector.
The M1 function must return y=M1\x.
   </para>
   <para>
The argument M1 can be the list (funM1,a1,a2,...).
In this case funM1, the first element in the list, must have the header:
<programlisting>
y = funM1 ( x , a1 , a2 , ... )
</programlisting>
where the input arguments a1, a2, ...
are automatically be appended at the end of the calling sequence.
   </para>
   <para>
The same feature is available for M2.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Test with Wilkinson-21+ matrix
n=21;
E = diag(ones(n-1,1),1);
m = (n-1)/2;
A = diag(abs(-m:m)) + E + E';
b = sum(A,2);
tol = 1e-12;
maxit = 15;
M1 = diag([10:-1:1 1 1:10]);
[x,flag,relres,iter,resvec] = mtlb_bicgstab(A,b,tol,maxit,M1)
xe = ones(n,1);

// Using bicgstab with a callback
n = 21;
tol = 1e-12;  maxit = 15;
function y = afun(x)
y = [0; x(1:n-1)] + ...
[((n-1)/2:-1:0)'; (1:(n-1)/2)'].*x + ...
[x(2:n); 0];
endfunction
function y = mfun(r)
y = r ./ [((n-1)/2:-1:1)'; 1; (1:(n-1)/2)'];
endfunction
b = afun(ones(n,1));
[x,flag,relres,iter,resvec] = mtlb_bicgstab(afun,b,tol,maxit,mfun)
xe = ones(n,1);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).</para>
   <para>http://www.netlib.org/templates/matlab/bicgstab.m</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst</member>
   <member>Copyright (C) 2005 - INRIA - Sage Group (IRISA)</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
