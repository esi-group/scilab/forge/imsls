<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from imsls_sor.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="imsls_sor" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>imsls_sor</refname><refpurpose>Solves linear equations using Successive Over-Relaxation Method.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   x  = imsls_sor(A, b)
   x  = imsls_sor(A, b, x0)
   x  = imsls_sor(A, b, x0, w)
   x  = imsls_sor(A, b, x0, w, max_it)
   x  = imsls_sor(A, b, x0, w, max_it, tol)
   [x, err]  = imsls_sor(...)
   [x, err, iter]  = imsls_sor(...)
   [x, err, iter, flag]  = imsls_sor(...)
   [x, err, iter, flag, res]  = imsls_sor(...)

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A:</term>
      <listitem><para>        a n-ny-n full or sparse matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>b:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles, right hand side vector</para></listitem></varlistentry>
   <varlistentry><term>x0:</term>
      <listitem><para>       a n-ny-1 full or sparse matrix of doubles, initial guess vector  (default: zeros(n,1))</para></listitem></varlistentry>
   <varlistentry><term>w:</term>
      <listitem><para>        a 1-ny-1 matrix of doubles, relaxation scalar (default w=0.4)</para></listitem></varlistentry>
   <varlistentry><term>max_it:</term>
      <listitem><para>   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)</para></listitem></varlistentry>
   <varlistentry><term>tol:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para>        a n-ny-1 full or sparse matrix of doubles,  solution vector</para></listitem></varlistentry>
   <varlistentry><term>err:</term>
      <listitem><para>      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.</para></listitem></varlistentry>
   <varlistentry><term>iter:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed</para></listitem></varlistentry>
   <varlistentry><term>flag:</term>
      <listitem><para>     a 1-ny-1 matrix of doubles, integer value, flag=0: solution found to tolerance, flag=1: no convergence given max_it</para></listitem></varlistentry>
   <varlistentry><term>res:</term>
      <listitem><para>      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Solves the linear system Ax=b using the
Successive Over-Relaxation Method (Gauss-Seidel method when omega = 1 ).
   </para>
   <para>
Any optional input argument equal to the empty matrix [] is replaced by its default value.
   </para>
   <para>
It is not possible to provide A as a function for imsls_jacobi.
This is because the solver internally splits the matrix A into M and N, then
uses these matrices directly.
   </para>
   <para>
Successive Overrelaxation (SOR) can be derived from the Gauss-Seidel method by
introducing an extrapolation parameter omega.
For the optimal choice of omega, SOR may converge faster than Gauss-Seidel by an order of magnitude.
   </para>
   <para>
If the coefficient matrix A is symmetric and positive definite, the SOR iteration
is guaranteed to converge for any value of omega between 0 and 2, though the choice of omega
can significantly affect the rate at which the SOR iteration converges.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// No convergence
A=imsls_makefish(4);
xe=(1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_sor(A,b)

// With initial guess
// No convergence
x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_sor(A,b,x0);

// Increase max_it: convergence
// Use default x0
w=0.8;
max_it=200;
tol=1000*%eps;
[x,err,iter,flag,res] = imsls_sor(A,b,[],w,max_it,tol);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).</para>
   <para>"Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).</para>
   <para>http://www.netlib.org/templates/matlab//sor.m</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst</member>
   <member>Copyright (C) 2005 - INRIA - Sage Group (IRISA)</member>
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>
