// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

//
// All parameters by default.
A=imsls_lehmer(16);
xe = (1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_pcg(A,b);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With x0
x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_pcg(A,b,x0);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With a matrix preconditionner M
// Convergence
M=eye(16,16);
max_it=100;
tol=1000*%eps;
[x,err,iter,flag,res] = imsls_pcg(A,b,x0,M,[],max_it,tol);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,21);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With a function preconditionner M
function y=precond(x)
    y=2*eye(16,16)\x
endfunction
[x,err,iter,flag,res] = imsls_pcg(A,b,x0,precond);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With a function Matrix-Vector A
function y=matvec(x)
    y=imsls_lehmer(16)*x
endfunction
[x,err,iter,flag,res] = imsls_pcg(matvec,b);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);

//
// With a function Matrix-Vector A and extra-args
//
function y=matvec2(x,A,B)
    y=(A+B)*x
endfunction
A = imsls_lehmer(16)/2;
B = A;
xe = (1:16)';
b=(A+B)*xe;
[x,err,iter,flag,res] = imsls_pcg(list(matvec2,A,B),b);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);

