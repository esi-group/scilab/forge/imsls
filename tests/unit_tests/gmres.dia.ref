// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->
A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_gmres(A,b,x0);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,6);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
M1=diag(diag(A));
M2=diag(diag(A));
max_it=16;
tol=1000*%eps;
rstr=20;
[x,err,iter,flag,res] = imsls_gmres(A,b,x0,M1,M2,rstr,max_it,tol);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,6);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
function y=precondM1(x)
    y=4*eye(16,16)\x
endfunction
function y=precondM2(x)
    y=4*eye(16,16)\x
endfunction
function y=matvec(x,transp)
    A = imsls_makefish(4)
    y=A*x
endfunction
[x,err,iter,flag,res] = imsls_gmres(matvec,b,x0,precondM1,precondM2,rstr,max_it,tol);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,6);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
[x,err,iter,flag,res] = imsls_gmres(matvec,b,x0,M1,M2,rstr,max_it,tol);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,6);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
[x,err,iter,flag,res] = imsls_gmres(A,b,x0,precondM1,precondM2,rstr,max_it,tol);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,6);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// Test with a nonsymetric matrix
//
n=110 ;
A=imsls_nonsym(-10,-1,n);
xe = (1:n)';
b=A*xe;
[x,err,iter,flag,res] = imsls_gmres(A,b);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-0);
// Do not check iter (perhaps varies depending on OS)
assert_checktrue(iter>100);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-10);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-10);
//
// With a function Matrix-Vector A and extra-args
//
function y=matvec2(x,A,B)
    y=(A+B)*x
endfunction
A = imsls_lehmer(16)/2;
B = A;
xe = (1:16)';
b=(A+B)*xe;
[x,err,iter,flag,res] = imsls_gmres(list(matvec2,A,B),b);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
