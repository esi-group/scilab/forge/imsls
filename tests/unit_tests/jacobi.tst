// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

//
// Test with a symetric matrix
//
A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_jacobi(A,b);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-1);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-2);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-2);
//
// Test with a symetric matrix
// Convergence
//
A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_jacobi(A,b,[],200);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-1);
assert_checkequal(iter,130);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-2);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-2);

//
A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_jacobi(A,b,x0);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-1);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-2);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-2);
//
// Increase iterations for convergence
max_it=100;
tol=1000*%eps;
[x,err,iter,flag,res] = imsls_jacobi(A,b,x0,max_it,tol);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-8);
assert_checkequal(iter,100);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-9);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-9);

//
// Test with a nonsymetric matrix
//
max_it=200;
n=110 ;
A=imsls_nonsym(-10,-1,n);
xe = (1:n)';
b=A*xe;
[x,err,iter,flag,res] = imsls_jacobi(A,b,[],max_it);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-0);
assert_checkequal(iter,200);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-2);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-2);

