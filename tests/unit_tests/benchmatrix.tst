// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- ENGLISH IMPOSED -->

A = imsls_wathen(7,7,0) ;
mprintf("matrix: Wathen\n");
n=max(size(A)) ;
M = eye(n,n) ;
M1 = eye(n,n) ;
M2 = eye(n,n) ;
max_it=200 ;
tol=1e-6 ;
x0=zeros(n,1);
xex=ones(n,1);
b=A*xex ;
restrt=50 ;
imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol,restrt);

// With default parameters
imsls_benchmatrix(A,b);

// With graphics
imsls_benchmatrix(A,b,x0,M,M1,M2,max_it,tol,restrt,%t);
