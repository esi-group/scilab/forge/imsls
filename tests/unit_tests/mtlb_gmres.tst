// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
x0=zeros(16,1);
[x,flag,relres,iter,resvec] = mtlb_gmres(A,b);
assert_checkequal(size(resvec,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,6);
assert_checkequal(flag,0);
assert_checktrue(relres<1.e-7);
assert_checktrue(resvec<=1);
assert_checktrue(resvec($)<1.e-7);
//
// Test with Wilkinson-21+ matrix
//
n=21;
E = diag(ones(n-1,1),1);
m = (n-1)/2;
A = diag(abs(-m:m)) + E + E';
b = sum(A,2);
tol = 1e-12;
maxit = 15;
M1 = diag([10:-1:1 1 1:10]);
restart = 10;
[x,flag,relres,iter,resvec] = mtlb_gmres(A,b,restart,tol,maxit,M1);
xe = ones(n,1);
assert_checkequal(size(resvec,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,20);
assert_checkequal(flag,0);
assert_checktrue(relres<1.e-7);
assert_checktrue(resvec<=1);
assert_checktrue(resvec($)<1.e-7);
//
// With callbacks
//
    function y = afun(x)
        y = [0; x(1:n-1)] + ...
              [((n-1)/2:-1:0)'; (1:(n-1)/2)'].*x + ...
              [x(2:n); 0];
    endfunction

    function y = mfun(r)
        y = r ./ [((n-1)/2:-1:1)'; 1; (1:(n-1)/2)'];
    endfunction

n = 21;
b = afun(ones(n,1));
tol = 1e-12;
maxit = 15;
restart = 10;
[x,flag,relres,iter,resvec] = mtlb_gmres(afun,b,10,tol,maxit,mfun);
xe = ones(n,1);
assert_checkequal(size(resvec,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,20);
assert_checkequal(flag,0);
assert_checktrue(relres<1.e-7);
assert_checktrue(resvec<=1);
assert_checktrue(resvec($)<1.e-7);

