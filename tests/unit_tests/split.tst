// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

//
// Splits for Jacobi
//
A=imsls_makefish(4);
b = zeros(16,1);
w = 1.0;
flag = 1;
[ M, N ] = imsls_split( A , b, w, flag );
assert_checkequal(A,M-N);
//
// Splits for SOR
//
A=imsls_makefish(4);
b = zeros(16,1);
w = 1.0;
flag = 2;
[ M, N , b ] = imsls_split( A , b, w, flag );
assert_checkequal(A,M-N);

