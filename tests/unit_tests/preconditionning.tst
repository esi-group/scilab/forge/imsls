// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


function printsummarry(name,x,err,iter,flag,res)
   mprintf("Solver=%s,",name)
   mprintf("  Iter=%d,",iter)
   mprintf("  Res=%f,",err)
   mprintf("  Flag=%d\n",flag)
endfunction

//
// Test preconditionning for methods which have M
// Should converge in 1 single iteration.
//
A=imsls_makefish(4);
C = chol(A);
xe=(1:16)';
b=A*xe;
M1 = A;
[x,err,iter,flag,res] = imsls_bicg(A,b,[],M1);
printsummarry("bicg",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_qmr(A,b,[],M1);
printsummarry("qmr",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_cheby(A,b,[],M1);
printsummarry("cheby",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,2);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_bicgstab(A,b,[],M1);
printsummarry("bicgstab",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_cgs(A,b,[],M1);
printsummarry("cgs",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_gmres(A,b,[],M1);
printsummarry("gmres",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_pcg(A,b,[],M1);
printsummarry("bicg",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);

//
// Test preconditionning for methods which have M1 and M2.
// Should converge in 1 single iteration.
//
A=imsls_makefish(4);
C = chol(A);
xe=(1:16)';
b=A*xe;
M1 = C';
M2 = C;
[x,err,iter,flag,res] = imsls_bicg(A,b,[],M1,M2);
printsummarry("bicg",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_qmr(A,b,[],M1,M2);
printsummarry("qmr",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_bicgstab(A,b,[],M1,M2);
printsummarry("bicgstab",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_cgs(A,b,[],M1,M2);
printsummarry("cgs",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_gmres(A,b,[],M1,M2);
printsummarry("gmres",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);
//
[x,err,iter,flag,res] = imsls_pcg(A,b,[],M1,M2);
printsummarry("bicg",x,err,iter,flag,res);
assert_checkequal(flag,0);
assert_checkequal(iter,1);
assert_checktrue(err<10*%eps);
assert_checkalmostequal(x,xe);


