// Copyright (C) 2008 - 2009 - INRIA - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
// Test with 2 input arguments and all output arguments
A=[10,1;1,10];
b=[11;11];
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b);
xexpected=[1;1];
assert_checkalmostequal ( xcomputed , xexpected , 1.e-7 );
assert_checkequal ( flag , 0 );
assert_checktrue ( err<%eps );
assert_checktrue ( iter <=2  );
assert_checkequal ( size(res) , [iter+1,1] );
assert_checkequal ( res($) , err );
//
// Test with 3 input arguments and all output arguments
A=[10,1;1,10];
b=[11;11];
tol = 100*%eps;
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,tol);
xexpected=[1;1];
assert_checkalmostequal ( xcomputed , xexpected , 1.e-7 );
assert_checktrue ( err<tol );
//
// Test with 4 input arguments and all output arguments
A=[10,1;1,10];
b=[11;11];
tol = 100*%eps;
maxit = 10;
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,tol,maxit);
xexpected=[1;1];
assert_checkalmostequal ( xcomputed , xexpected , 1.e-7 );
//
// Test with 5 input arguments and all output arguments
A=[10,1;1,10];
b=[11;11];
tol = 100*%eps;
maxit = 10;
M1=[1,0;0,1];
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,tol,maxit,M1);
xexpected=[1;1];
assert_checkalmostequal ( xcomputed , xexpected , 1.e-7 );
//
// Test with 6 input arguments and all output arguments
A=[10,1;1,10];
b=[11;11];
tol = 100*%eps;
maxit = 10;
M1=[1,0;0,1];
M2=[1,0;0,1];
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,tol,maxit,M1,M2);
xexpected=[1;1];
assert_checkalmostequal ( xcomputed , xexpected , 1.e-7 );
//
// Test with 7 input arguments and all output arguments
A=[10,1;1,10];
b=[11;11];
tol = 100*%eps;
maxit = 10;
M1=[1,0;0,1];
M2=[1,0;0,1];
x0=[1;1];
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,tol,maxit,M1,M2,x0);
xexpected=[1;1];
assert_checkalmostequal ( xcomputed , xexpected , 1.e-7 );
//
// Test the special case where b=0
A=[100,1;1,10.];
b=[0;0];
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b);
xexpected=[0;0];
assert_checkalmostequal ( xcomputed , xexpected , 1.e-7 );
assert_checkequal ( flag , 0 );
assert_checktrue ( err<%eps );
assert_checkequal ( iter , 0 );
//
// Try a hard case where preconditionning is necessary
// This is the Hilbert 5x5 matrix : A = 1/(testmatrix("hilb",5))
A = [
1.                          0.5000000000000001110223    0.3333333333333334258519    0.2500000000000000555112    0.2000000000000000666134
0.4999999999999982236432    0.3333333333333319825620    0.2499999999999988897770    0.1999999999999990951682    0.1666666666666659080143
0.3333333333333320380731    0.2499999999999990563104    0.1999999999999992617017    0.1666666666666660745477    0.1428571428571423496123
0.2499999999999990285549    0.1999999999999993449684    0.1666666666666661855700    0.1428571428571424883902    0.1249999999999996808109
0.1999999999999991506794    0.1666666666666660745477    0.1428571428571424328791    0.1249999999999996669331    0.11111111111111082739
];
b = ones(5,1);
// If M is not set, the PCG does not work.
// So this test makes sure that M is correctly used.
M=A;
// Matrix A is not exactly symmetric, which causes a warning.
warning("off");
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,%eps,3,M );
warning("on");
expected = [
5.
-120.
630.
-1120.
630.
];
assert_checkalmostequal ( xcomputed , expected, 1.e8 * %eps );
assert_checkequal ( flag , 0 );
assert_checkequal ( iter , 2 );
//
// Try a difficult case where preconditionning is necessary
// Use two pre-conditionning matrices.
// This is the Hilbert 5x5 matrix : A = 1/(testmatrix("hilb",5))
A = [
1.                          0.5000000000000001110223    0.3333333333333334258519    0.2500000000000000555112    0.2000000000000000666134
0.4999999999999982236432    0.3333333333333319825620    0.2499999999999988897770    0.1999999999999990951682    0.1666666666666659080143
0.3333333333333320380731    0.2499999999999990563104    0.1999999999999992617017    0.1666666666666660745477    0.1428571428571423496123
0.2499999999999990285549    0.1999999999999993449684    0.1666666666666661855700    0.1428571428571424883902    0.1249999999999996808109
0.1999999999999991506794    0.1666666666666660745477    0.1428571428571424328791    0.1249999999999996669331    0.11111111111111082739
];
b = ones(5,1);
// This is the cholesky factorization of the matrix A : C = chol(A)
C = [
1.    0.5000000000000001110223    0.3333333333333334258519    0.2500000000000000555112    0.2000000000000000666134
0.    0.288675134594810367528     0.2886751345948112557060    0.2598076211353305131624    0.2309401076758494653074
0.    0.                          0.0745355992499937836104    0.1118033988749897594817    0.1277753129999876502421
0.    0.                          0.                          0.0188982236504644136865    0.0377964473009222076683
0.    0.                          0.                          0.                          0.0047619047619250291087
];
M1 = C';
M2 = C;
// Matrix A is not exactly symmetric, which causes a warning.
warning("off");
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b , %eps,3 , M1 , M2 );
warning("on");
expected = [
5.
-120.
630.
-1120.
630.
];
assert_checkalmostequal ( xcomputed , expected, 1.e8 * %eps );
assert_checkequal ( flag , 0 );
assert_checkequal ( iter , 2 );
// Numerical tests
// Case where A is sparse
A=[ 94  0   0   0    0   28  0   0   32  0
0   59  13  5    0   0   0   10  0   0
0   13  72  34   2   0   0   0   0   65
0   5   34  114  0   0   0   0   0   55
0   0   2   0    70  0   28  32  12  0
28  0   0   0    0   87  20  0   33  0
0   0   0   0    28  20  71  39  0   0
0   10  0   0    32  0   39  46  8   0
32  0   0   0    12  33  0   8   82  11
0   0   65  55   0   0   0   0   11  100];
b = [154.
87.
186.
208.
144.
168.
158.
135.
178.
231.];
Asparse = sparse(A);
// With the default 10 iterations, the algorithm performs well
[xcomputed, flag, err, iter, res]=mtlb_pcg(Asparse,b);
xexpected=ones(10,1);
assert_checkalmostequal ( xcomputed,xexpected,1.e-5);
assert_checkequal ( flag , 0 );
assert_checkequal ( iter , 9 );
assert_checktrue ( err<1.e-6 );
// Numerical tests
//Well conditionned problem
A=[ 94  0   0   0    0   28  0   0   32  0
0   59  13  5    0   0   0   10  0   0
0   13  72  34   2   0   0   0   0   65
0   5   34  114  0   0   0   0   0   55
0   0   2   0    70  0   28  32  12  0
28  0   0   0    0   87  20  0   33  0
0   0   0   0    28  20  71  39  0   0
0   10  0   0    32  0   39  46  8   0
32  0   0   0    12  33  0   8   82  11
0   0   65  55   0   0   0   0   11  100];
b = [154.
87.
186.
208.
144.
168.
158.
135.
178.
231.];
// With the default 10 iterations, the algorithm performs well
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b);
xexpected=ones(10,1);
assert_checkalmostequal ( xcomputed,xexpected,1.e-5);
assert_checkequal ( flag , 0 );
assert_checkequal ( iter , 9 );
assert_checktrue ( err<1.e-6 );
// With a tolerance of 1.e-3, there are 5 iterations and the status is "sucess"
tol=1.d-3;
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,tol);
assert_checkequal ( flag , 0 );
assert_checktrue ( iter < 10 );
// With a tolerance of %eps but only 5 iterations allowed, the status is "fail"
tol=%eps;
maxIter = 5;
[xcomputed, flag, err, iter, res]=mtlb_pcg(A,b,tol,maxIter);
WARNING: imsls_pcg: Algorithm has not converged.
assert_checkequal ( flag , 1 );
assert_checkequal ( iter , maxIter );
// Numerical tests
// Case where A is given as a function computing the right-hand side
mymatrix=[ 94  0   0   0    0   28  0   0   32  0
0   59  13  5    0   0   0   10  0   0
0   13  72  34   2   0   0   0   0   65
0   5   34  114  0   0   0   0   0   55
0   0   2   0    70  0   28  32  12  0
28  0   0   0    0   87  20  0   33  0
0   0   0   0    28  20  71  39  0   0
0   10  0   0    32  0   39  46  8   0
32  0   0   0    12  33  0   8   82  11
0   0   65  55   0   0   0   0   11  100];
b = [154.
87.
186.
208.
144.
168.
158.
135.
178.
231.];
function y=Atimesx(x,mymatrix)
    y=mymatrix*x
endfunction
// With the default 10 iterations, the algorithm performs well
Alist = list(Atimesx,mymatrix);
[xcomputed, flag, err, iter, res]=mtlb_pcg(Alist,b);
xexpected=ones(10,1);
assert_checkalmostequal ( xcomputed,xexpected,1.e-5);
assert_checkequal ( flag , 0 );
assert_checkequal ( iter , 9 );
assert_checktrue ( err<1.e-6 );
// Numerical tests
// Case where A is given as a function computing the right-hand side
b = [154.
87.
186.
208.
144.
168.
158.
135.
178.
231.];
function y=Afunction(x)
    mymatrix=[ 94  0   0   0    0   28  0   0   32  0
    0   59  13  5    0   0   0   10  0   0
    0   13  72  34   2   0   0   0   0   65
    0   5   34  114  0   0   0   0   0   55
    0   0   2   0    70  0   28  32  12  0
    28  0   0   0    0   87  20  0   33  0
    0   0   0   0    28  20  71  39  0   0
    0   10  0   0    32  0   39  46  8   0
    32  0   0   0    12  33  0   8   82  11
    0   0   65  55   0   0   0   0   11  100];
    y=mymatrix*x
endfunction
// With the default 10 iterations, the algorithm performs well
[xcomputed, fail, err, iter, res]=mtlb_pcg(Afunction,b);
xexpected=ones(10,1);
assert_checkalmostequal ( xcomputed,xexpected,1.e-5);
assert_checkequal ( flag , 0 );
assert_checkequal ( iter , 9 );
assert_checktrue ( err<1.e-6 );
