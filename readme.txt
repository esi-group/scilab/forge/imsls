Scilab Iterative Methods for Sparse Linear Equations Toolbox

Purpose
-------

Imsls provides iterative methods for sparse linear systems of equations.
More precisely, it provides functions to find x such that A*x=b,
where A is a n-by-n matrix of doubles and b is a n-by-1
matrix of doubles.
Although these functions can manage full matrices, it is mainly
designed for sparse matrices.

This module is mainly a Scilab port of the Matlab scripts provided
on netlib.org/templates.

One of the interesting point here is that the matrix-vector products
or the preconditionning steps M\x can be performed either with full or sparse
matrices, or with callback functions.
This flexibility makes the module convenient to use in situations when
the sparse matrices are not stored in memory, since only the matrix-vector
product (or the preconditionning step M\x) is required.

Moreover, we provide Matlab-compatible pcg and qmr solvers:
 * the order of the arguments are the same as in Matlab,
 * the default values of the Matlab functions are the same as in Matlab,
 * the headers of the callback functions are the same as in Matlab.
This contrasts with Scilab's internal functions, where the two last points are
completely unsatisfied.

Finally, we provide a complete test suite for these functions, which
are using robust argument checking.

Features
--------

 * imsls_bicg: BIConjugate Gradient method
 * imsls_bicgstab: BIConjugate Gradient STABilized method
 * imsls_pcg: Conjugate Gradient method
 * imsls_cgs: Conjugate Gradient Squared method
 * imsls_cheby: CHEBYshev method
 * imsls_gmres: Generalized Minimal RESidual method
 * imsls_jacobi: JACOBI method
 * imsls_qmr: Quasi Minimal Residual method
 * imsls_sor: Successive Over-Relaxation method

Support:
 * imsls_benchmatrix : Test a matrix against all solvers.
 * imsls_getpath : Returns the path to the current module.
 * imsls_lehmer : Returns the Lehmer matrix.
 * imsls_makefish : Returns the Poisson matrix.
 * imsls_matgen : Returns a test matrix.
 * imsls_nonsym : Returns a non symetric matrix.
 * imsls_spdiags : Extract and create sparse band and diagonal matrices
 * imsls_split : Sets up the matrix splitting for Jacobi and SOR.
 * imsls_tester : Test all algorithms
 * imsls_wathen : Generates a random finite element matrix.

Compatibility
 * mtlb_bicg : Solves linear equations using BiConjugate Gradient Method with preconditioning.
 * mtlb_bicgstab : Solves linear equations using BiConjugate Gradient Stabilized Method with preconditioning.
 * mtlb_cgs : Solves linear equations using Conjugate Gradient Squared Method with preconditioning.
 * mtlb_gmres : Solves linear equations using Generalized Minimal residual with restarts .
 * mtlb_pcg : Solves linear equations using Conjugate Gradient method with preconditioning.
 * mtlb_qmr : Solves linear equations using Quasi Minimal Residual method with preconditioning.

Dependencies
------------

 * This module depends on the helptbx module (to build the help pages).
 * This module depends on the apifun module, v0.2.
 * This module depends on the assert module.


History
-------

In 1993, the Book "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods"
was published.
Associated source codes in the Matlab language were provided at http://www.netlib.org/templates/.

From 2000 to 2006, these function have been ported to Scilab 4 by Aladin Group (IRISA-INRIA).

Starting in 2010, Michael Baudin upgraded this module to Scilab 5.
I updated the management of the input arguments, the arguments checking and the
management of the callbacks (for the matrix-vector product <literal>A*x</literal> and
the preconditionning <literal>M\x</literal>).
The argument management is now based on apifun, which greatly improves the robustness and
simplifies the writing of the functions.
I described the input arguments more precisely in the help pages, added examples and created unit tests
(based on the assert module).
I added the right preconditionner M2 for all functions.
I created the Matlab compatibility functions.

TODO
----

Extra-TODOS:
 * Create a Scilab port of LSQR: http://www.stanford.edu/group/SOL/software/lsqr.html



Licence
-------

This toolbox is released under the terms of the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Authors
-------

 * 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
 * 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
 * 2000 - 2001 - INRIA - Aladin Group
 * 2010 - 2011 - DIGITEO - Michael Baudin

Bibliography
------------

 * http://www.irisa.fr/aladin/codes/SCILIN/
 * http://www.netlib.org/templates/
 * http://graal.ens-lyon.fr/~jylexcel/scilab-sparse/meeting07/
 * Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
 * "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
 * "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).


